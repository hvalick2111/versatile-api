package versatile.api.render;

public interface IRender {
    void render();
}
