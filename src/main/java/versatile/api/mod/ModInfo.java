package versatile.api.mod;

import versatile.api.game.lang.IDisplayName;

public interface ModInfo {
    String name();

    String version();

    String[] authors();

    IDisplayName description();
}
