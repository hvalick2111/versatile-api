package versatile.api.mod;

import versatile.api.game.lang.IDisplayName;

public interface InfoEnvironment {
    ModInfo createInfo(String name);
    ModInfo createInfo(String name, String version);
    ModInfo createInfo(String name, String version, String description, String... authors);
    ModInfo createInfo(String name, String version, IDisplayName description, String... authors);
}
