package versatile.api.resource.model;

public interface RenderModel {
    void create();
    void render();
    void delete();
}
