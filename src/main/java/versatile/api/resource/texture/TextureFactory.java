package versatile.api.resource.texture;

@FunctionalInterface
public interface TextureFactory {
    RenderTexture create(TextureManager manager) throws Exception;
}
