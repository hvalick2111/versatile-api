package versatile.api.internal;

import versatile.api.game.block.data.TagSerialization;

/*
 * Internal api class
 */
public class INSerializationFactory {
    private static TagSerialization serializationBase;


    public static void initSerializationBase(TagSerialization base) {
        if(serializationBase == null)
            serializationBase = base;
        else
            throw new RuntimeException("Tag serialization base initialized!");
    }

    public static TagSerialization serializationBase() {
        if(serializationBase == null)
            throw new RuntimeException("Tag serialization base was not initialized!");
        return serializationBase;
    }
}
