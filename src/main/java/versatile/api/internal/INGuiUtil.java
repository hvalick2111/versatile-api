package versatile.api.internal;

public class INGuiUtil {
    private static float defaultSlotSize = -1f;

    public static void setDefaultSlotSize(float defaultSlotSize) {
        if (INGuiUtil.defaultSlotSize >= 0) throw new RuntimeException("Default slot size already initialized");
        if (defaultSlotSize <= 0) throw new RuntimeException("Good joke, default slot size must be positive");
        INGuiUtil.defaultSlotSize = defaultSlotSize;
    }

    public static float getDefaultSlotSize() {
        if (INGuiUtil.defaultSlotSize < 0) throw new RuntimeException("Default slot size was not initialized");
        return defaultSlotSize;
    }
}
