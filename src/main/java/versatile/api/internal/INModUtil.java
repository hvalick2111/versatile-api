package versatile.api.internal;

import versatile.api.IMod;
import versatile.api.Modification;

import java.util.function.Function;

/*
 * Internal api class
 */
public class INModUtil {
    private static Function<IMod, Modification> modMapper;


    public static void initModMapper(Function<IMod, Modification> mapper) {
        if(modMapper == null)
            modMapper = mapper;
        else
            throw new RuntimeException("Mod mapper already initialized!");
    }

    public static Modification map(IMod mod) {
        if(modMapper == null)
            throw new RuntimeException("Mod mapper was not initialized!");
        return modMapper.apply(mod);
    }
}
