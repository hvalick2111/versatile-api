package versatile.api.game.entity;

import versatile.api.game.block.BlockInfo;

@FunctionalInterface
public interface EntityHandlerInitializer {
    void registerHandler(BlockInfo info, EntityHandler<?> handler);


    default <T extends EntityContainer> void registerHandler(VanillaEntity<T> e, EntityHandler<T> handler) {
        e.ifPresent(info -> registerHandler(info, handler));
    }
}
