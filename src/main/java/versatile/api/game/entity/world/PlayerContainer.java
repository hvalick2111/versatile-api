package versatile.api.game.entity.world;

import versatile.api.game.GameSide;
import versatile.api.game.entity.EntityContainer;
import versatile.api.game.entity.Hand;
import versatile.api.game.gui.GuiInventory;
import versatile.api.game.inventory.Inventory;
import versatile.api.game.item.ItemContainer;

import java.util.function.Consumer;

public interface PlayerContainer extends EntityContainer {
    ItemContainer getHeldItem(Hand hand);

    /**
     * Copies the {@code newHeldItem}, and places it
     * to entity's {@code hand}
     */
    void setHeldItem(Hand hand, ItemContainer newHeldItem);


    /**
     * Opens gui container for this player with items in {@code inventory}.
     * Opened container is a default container with linear rows of slots who
     * items are linked to the corresponding indexes in the {@code inventory}.
     * The number of slots in the container is equal to the {@link Inventory#size()}
     * of the {@code inventory}
     */
    default void openInventory(Inventory inventory) {
        openInventory(inventory, gui -> {
        });
    }

    /**
     * Opens gui container for this player with items in {@code inventory}.
     * Opened container is a container with configuration in {@code setting}.
     * @param effective if it == client, the setting will be applied on the
     *                  client side and server side independently of each other.
     *                  Otherwise, the configuration will be applied on the
     *                  server side, serialized, and sent to the client
     */
    void openInventory(Inventory inventory, Consumer<GuiInventory> setting, GameSide effective);

    /**
     * Opens gui container for this player with items in {@code inventory}.
     * Opened container is a container with configuration in {@code setting}.
     */
    default void openInventory(Inventory inventory, Consumer<GuiInventory> setting) {
        openInventory(inventory, setting, GameSide.SERVER);
    }

    /**
     * Similar with {@link #openInventory(Inventory)}, but the
     * player can't interact with the contents of the {@code inventory}
     */
    default void viewInventory(Inventory inventory) {
        openInventory(inventory, GuiInventory::setNoInteract);
    }
}
