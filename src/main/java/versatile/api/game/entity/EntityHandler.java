package versatile.api.game.entity;

import versatile.api.game.world.WorldController;

public interface EntityHandler<T extends EntityContainer> {
    default boolean entityJoinWorld(WorldController world, T info){
        return true;
    }
}
