package versatile.api.game.entity;

import versatile.api.game.world.Pos3d;
import versatile.api.game.world.Vec2d;

public interface EntityObject {
    Pos3d getPos();
    void moveTo(Pos3d pos);

    Vec2d getSize();
}
