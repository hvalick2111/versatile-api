package versatile.api.game.entity;

import versatile.api.game.OptionalInfo;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.entity.world.PlayerContainer;
import versatile.api.game.entity.world.ZombieContainer;
import versatile.api.internal.INEntityRegistry;

public class VanillaEntity<T extends EntityContainer> implements OptionalInfo {
    public static VanillaEntity<PlayerContainer> PLAYER = new VanillaEntity<>(find("player"));
    public static VanillaEntity<ZombieContainer> ZOMBIE = new VanillaEntity<>(find("zombie"));
    public static VanillaEntity<?> SKELETON = new VanillaEntity<>(find("skeleton"));

    private BlockInfo entityInfo;

    private VanillaEntity(BlockInfo entityInfo) {
        this.entityInfo = entityInfo;
    }

    @Override
    public BlockInfo orEmpty() {
        if (entityInfo != null)
            return entityInfo;
        return BlockInfo.EMPTY;
    }


    /*Internal method*/
    private static BlockInfo find(String name) {
        return INEntityRegistry.vanillaEntities().get(name);
    }

}
