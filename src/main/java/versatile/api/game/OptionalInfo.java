package versatile.api.game;

import versatile.api.game.block.BlockInfo;

import java.util.function.Consumer;

public interface OptionalInfo {
    BlockInfo orEmpty();

    default boolean isPresent() {
        return !orEmpty().isEmpty();
    }

    default boolean is(BlockInfo info) {
        BlockInfo orEmpty = orEmpty();
        if(orEmpty.isEmpty())
            return false;
        return orEmpty.equals(info);
    }

    default void ifPresent(Consumer<BlockInfo> presentedBlock){
        BlockInfo orEmpty = orEmpty();
        if(!orEmpty.isEmpty()) {
            presentedBlock.accept(orEmpty);
        }
    }

    default boolean isAll(BlockInfo... infos) {
        for (BlockInfo info : infos)
            if (!is(info))
                return false;
        return true;
    }

    static boolean isPresent(OptionalInfo... blocks) {
        for (OptionalInfo block : blocks)
            if (!block.isPresent())
                return false;
        return true;
    }
}
