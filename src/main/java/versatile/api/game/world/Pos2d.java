package versatile.api.game.world;

public final class Pos2d {
    private final double x, y;

    public Pos2d(Number x, Number y) {
        this.x = x.doubleValue();
        this.y = y.doubleValue();
    }


    public Pos2d() {
        x = y = 0;
    }

    public Pos2d expand(Number x, Number y){
        return new Pos2d(x() + x.doubleValue(), y() + y.doubleValue());
    }

    public Pos2d expandX(Number x){
        return expand(x, 0);
    }

    public Pos2d expandY(Number y){
        return expand(0, y);
    }

    public Pos2d expandZ(Number z){
        return expand(0, 0);
    }

    public double x() {
        return x;
    }

    public double y() {
        return y;
    }

    public Pos clampToPos(int yLevel){
        return new Pos(x, yLevel, y);
    }
}
