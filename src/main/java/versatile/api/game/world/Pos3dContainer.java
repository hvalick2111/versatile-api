package versatile.api.game.world;

import com.google.gson.annotations.SerializedName;

public final class Pos3dContainer {
    @SerializedName("pos")
    private Pos3d pos;

    public Pos3dContainer(Pos3d pos) {
        this.pos = pos;
    }

    public void setPos(Pos3d pos) {
        this.pos = pos;
    }

    public Pos3d getPos() {
        return pos;
    }
}
