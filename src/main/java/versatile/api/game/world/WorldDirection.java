package versatile.api.game.world;

public enum WorldDirection {
    DOWN,
    UP,
    NORTH,
    SOUTH,
    WEST,
    EAST

}
