package versatile.api.game.lang;

@FunctionalInterface
public interface IDisplayName {
    /**
     * Not cached the name
     * */
    String deduceName(LangManager manager);
}
