package versatile.api.game.gui;

import versatile.api.game.block.data.DataCapacitor;
import versatile.api.game.block.data.TagCompound;
import versatile.api.game.block.data.TagSerialization;
import versatile.api.game.inventory.Inventory;
import versatile.api.game.item.ItemContainer;

import java.io.Serializable;
import java.util.function.Predicate;

public class GuiSlot implements DataCapacitor {
    private transient final Inventory container;

    private SlotMask mask;

    private boolean visible = true;
    private double x, y;
    private int slotIndex;

    GuiSlot(Inventory container) {
        this.container = container;
    }

    public GuiSlot(double x, double y, int slotIndex, Inventory container) {
        this.x = x;
        this.y = y;
        this.slotIndex = slotIndex;
        this.container = container;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public ItemContainer getContained() {
        return container.getItem(slotIndex);
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void setMask(SlotMask mask){
        this.mask = mask;
    }

    public boolean canPutItem(ItemContainer container){
        return mask == null || mask.test(container);
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public void restoreDataFrom(TagCompound tag) {
        x = tag.getDoubleOrDefault("x", 0);
        y = tag.getDoubleOrDefault("y", 0);
        slotIndex = tag.getIntOrDefault("slot_index", -1);
        visible = tag.getBooleanOrDefault("visible", true);

        mask = TagSerialization.base().deserializeOrNull(tag, "mask", SlotMask.class);

        if(slotIndex < 0)
            throw new RuntimeException("Slot index not defined");
    }

    @Override
    public void saveDataTo(TagCompound tag) {
        tag.set("x", x);
        tag.set("y", y);
        tag.set("slot_index", slotIndex);
        tag.set("visible", visible);

        tag.set("mask", TagSerialization.base().serialize(mask));
    }

    public interface SlotMask extends Predicate<ItemContainer>, Serializable {}
}