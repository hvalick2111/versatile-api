package versatile.api.game.block;

import versatile.api.game.OptionalBlockInfo;

import java.util.function.Consumer;

@FunctionalInterface
public interface BlockHandlerInitializer {
    void registerHandler(BlockHandler handler, BlockInfo info);


    default void registerHandler(BlockHandler handler,OptionalBlockInfo... blocks) {
        Consumer<BlockInfo> consumer = info -> registerHandler(handler, info);
        for (OptionalBlockInfo b: blocks)
            b.ifPresent(consumer);
    }
}
