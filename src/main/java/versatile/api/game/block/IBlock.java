package versatile.api.game.block;

import versatile.api.game.lang.IDisplayName;
import versatile.api.resource.texture.TextureFactory;

public interface IBlock {
    BlockInfo getSource();

    IDisplayName getDisplayName();

    TextureFactory getTexture();

    BlockMaterial getMaterial();

    boolean isCreateItemBlock();

    HarvestType getHarvestType();

    int getHarvestTier();

    float getHardness();
}
