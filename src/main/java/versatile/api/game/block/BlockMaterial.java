package versatile.api.game.block;

public enum BlockMaterial {
    WOOD,
    GROUND,
    METAL,
    ROCK,
    LIQUID,
    AIR,
}
