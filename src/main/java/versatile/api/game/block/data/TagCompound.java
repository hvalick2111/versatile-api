package versatile.api.game.block.data;

import versatile.api.game.block.BlockInfo;
import versatile.api.game.item.ItemContainer;
import versatile.api.game.world.Pos;
import versatile.api.game.world.Pos3d;
import versatile.api.internal.INTagUtil;

import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

//TODO: delete this annotation
@SuppressWarnings("unused")
public interface TagCompound extends Cloneable {
    /**
     * @return A new empty TagCompound
     */
    static TagCompound create() {
        return INTagUtil.emptyTag();
    }

    /**
     * @return A TagCompound with DataCapacitor's data tag
     */
    static TagCompound createFrom(DataCapacitor capacitor) {
        TagCompound compound = create();
        compound.readFrom(capacitor);
        return compound;
    }

    /**
     * @return true if tag has no keys
     */
    default boolean isEmpty(){
        return size() == 0;
    }


    /**
     * @return The {@link BlockInfo} associated with the {@code key}, or
     * {@link ItemContainer#EMPTY} if this tag contains no mapping for the key.
     */
    default BlockInfo getInfoOrEmpty(String key){
        if(contains(key))
            return new BlockInfo(getStringOrDefault(key, ""));
        return BlockInfo.EMPTY;
    }

    /**
     * @return The {@link TagCompound} associated with the {@code key}, or
     * empty tag if this tag contains no mapping for the key.
     */
    TagCompound getTagOrEmpty(String key);

    /**
     * @return The {@link TagCompound} associated with the {@code key}, or
     * {@code defaultValue} if this tag contains no mapping for the key.
     */
    TagCompound getTagOrDefault(String key, TagCompound defaultValue);

    /**
     * Restores {@code dest} from this tag.
     * Usually delegates to {@link DataCapacitor#restoreDataFrom(TagCompound)}
     * @param dest the capacitor which will be read by this tag
     */
    default void writeInto(DataCapacitor dest){
        dest.restoreDataFrom(this);
    }

    void extractFrom(TagCompound reader);

    boolean removeKey(String key);

    default void readFrom(DataCapacitor src){
        src.saveDataTo(this);
    }

    TagCompound getTagOrFilled(String key, Consumer<TagCompound> defaultValue);

    default void set(String key, BlockInfo value){
        set(key, value.toString());
    }

    default void set(String key, Pos value){
        set(key + "$_element_x", value.x());
        set(key + "$_element_y", value.y());
        set(key + "$_element_z", value.z());
    }

    default Pos getPosOrDefault(String key, Pos defaultValue){
        if(!contains(key + "$_element_x") || !contains(key + "$_element_y") || !contains(key + "$_element_z"))
            return defaultValue;
        int x = getIntOrDefault(key + "$_element_x", 0);
        int y = getIntOrDefault(key + "$_element_y", 0);
        int z = getIntOrDefault(key + "$_element_z", 0);
        return new Pos(x, y, z);
    }

    default void set(String key, Pos3d value){
        set(key + "$_element_x", value.x());
        set(key + "$_element_y", value.y());
        set(key + "$_element_z", value.z());
    }

    default Pos3d getPos3dOrDefault(String key, Pos3d defaultValue){
        if(!contains(key + "$_element_x") || !contains(key + "$_element_y") || !contains(key + "$_element_z"))
            return defaultValue;
        double x = getDoubleOrDefault(key + "$_element_x", 0);
        double y = getDoubleOrDefault(key + "$_element_y", 0);
        double z = getDoubleOrDefault(key + "$_element_z", 0);
        return new Pos3d(x, y, z);
    }

    boolean contains(String key);

    Set<String> keys();

    int size();

    TagCompound clone();

    <T> void setArray(String key, T[] array, Function<T, TagCompound> ser);

    <T> T[] getArrayOrDefault(String key, Function<TagCompound, T> deSer, T[] defaultValue);


    String getStringOrDefault(String key, String defaultValue);

    String getStringOrEmpty(String key);

    int getIntOrDefault(String key, int defaultValue);

    boolean getBooleanOrDefault(String key, boolean defaultValue);

    float getFloatOrDefault(String key, float defaultValue);

    double getDoubleOrDefault(String key, double defaultValue);

    long getLongOrDefault(String key, long defaultValue);

    int[] getIntsOrDefault(String key, int[] defaultValue);

    int[] getIntsOrEmpty(String key);

    byte[] getBytesOrDefault(String key, byte[] defaultValue);

    byte[] getBytesOrEmpty(String key);

    String getStringOrDefault(String key, Supplier<String> defaultValue);

    int getIntOrDefault(String key, Supplier<Integer> defaultValue);

    boolean getBooleanOrDefault(String key, Supplier<Boolean> defaultValue);

    float getFloatOrDefault(String key, Supplier<Float> defaultValue);

    double getDoubleOrDefault(String key, Supplier<Double> defaultValue);

    long getLongOrDefault(String key, Supplier<Long> defaultValue);

    int[] getIntsOrDefault(String key, Supplier<int[]> defaultValue);

    byte[] getBytesOrDefault(String key, Supplier<byte[]> defaultValue);

    void set(String key, String value);

    void set(String key, TagCompound value);

    void set(String key, int value);

    void set(String key, boolean value);

    void set(String key, float value);

    void set(String key, double value);

    void set(String key, long value);

    void set(String key, int[] value);

    void set(String key, byte[] value);

    @Override
    boolean equals(Object object);

    @Override
    int hashCode();
}
