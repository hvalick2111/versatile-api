package versatile.api.game.block;

import versatile.api.game.entity.Hand;
import versatile.api.game.entity.world.PlayerContainer;
import versatile.api.game.world.Pos;
import versatile.api.game.world.WorldController;

@SuppressWarnings("unused")
public interface BlockHandler {
    default boolean breakBlock(WorldController world, Pos pos) {
        return true;
    }

    default boolean blockActivated(WorldController world, Pos pos, PlayerContainer clicker, Hand clickHand) {
        return false;
    }

    default boolean placeBlock(WorldController world, Pos placingPos, PlayerContainer placer) {
        return true;
    }
}
