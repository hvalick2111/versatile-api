package versatile.api.game.block;

public enum HarvestType {
    PICKAXE,
    AXE,
    SHOVEL,
    NONE
}
