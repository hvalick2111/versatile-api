package versatile.api.game.inventory;

import versatile.api.game.OptionalItemInfo;
import versatile.api.game.block.data.DataCapacitor;
import versatile.api.game.block.data.TagCompound;
import versatile.api.game.item.ItemContainer;

public interface Inventory extends DataCapacitor, Cloneable {
    /**
     * @return constant size of the inventory slots
     */
    int size();

    /**
     * @param i between the inventory {@link #size()} and zero
     * @return non-null item in the <tt>i</tt> slot
     */
    ItemContainer getItem(int i);


    /**
     * Sets copied <tt>item</tt> in slot <tt>i</tt>
     *
     * @param i    between the inventory {@link #size()} and zero
     * @param item non-null
     * @return the previous item in slot <tt>i</tt>
     */
    ItemContainer setItem(int i, ItemContainer item);

    /**
     * Sets the new {@link ItemContainer} of <tt>itemPattern</tt> in slot <tt>i</tt>
     *
     * @param i  between the inventory {@link #size()} and zero
     * @param itemPattern non-null
     * @return the previous item in slot <tt>i</tt>
     */
    default ItemContainer setItem(int i, OptionalItemInfo itemPattern){
        return setItem(i, new ItemContainer(itemPattern));
    }

    /**
     * @param i between the inventory {@link #size()} and zero
     * @return true if slot <tt>i</tt> contains non-empty item
     */
    default boolean containsItem(int i) {
        return !getItem(i).isEmpty();
    }


    /**
     * Collects all inventory item in array
     *
     * @return result array of non-null inventory items
     */
    default ItemContainer[] collectInArray() {
        int size = size();
        ItemContainer[] containers = new ItemContainer[size];
        for (int i = 0; i < size; i++) containers[i] = getItem(i);
        return containers;
    }

    default boolean isEmpty() {
        boolean isEmpty = true;
        for (int i = 0; i < size(); i++)
            if (!getItem(i).isEmpty())
                isEmpty = false;
        return isEmpty;
    }

    @Override
    default void restoreDataFrom(TagCompound tag) {
        ItemContainer[] inv = tag.getArrayOrDefault("inventory", ItemContainer::deserialize, new ItemContainer[0]);
        for (int i = 0; i < Math.min(size(), inv.length); i++)
            setItem(i, inv[i]);
    }

    @Override
    default void saveDataTo(TagCompound tag) {
        tag.setArray("inventory", collectInArray(), TagCompound::createFrom);
    }

//    default Inventory clone() throws CloneNotSupportedException {
//        try {
//            Inventory cloned = (Inventory) DataCapacitor.super.clone();
//            for (int i = 0; i < cloned.size(); i++)
//                cloned.setItem(i, getItem(i).clone());
//            return cloned;
//        } catch (CloneNotSupportedException e) {
//            throw new RuntimeException(e);
//        }
//    }
}
