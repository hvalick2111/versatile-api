package versatile.api.game.item;

import versatile.api.game.block.BlockInfo;

import java.util.function.Consumer;

public interface ItemInitializer {
    void registerItem(BlockInfo source, Consumer<ItemFactory> factory);
    BlockInfo registerItem(String uniqueName, Consumer<ItemFactory> factory);
}