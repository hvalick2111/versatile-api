package versatile.api.game.item;

import versatile.api.game.block.data.TagCompound;
import versatile.api.internal.INItemUtil;

/*
 * Internal api class
 */
public class _INItemWrapper {
    public static void setCallBack(ItemContainer container, INItemUtil.INItemCallback callback){
        container.internalCallback = callback;
    }

    public static INItemUtil.INItemCallback readCallBack(ItemContainer container){
        return container.internalCallback;
    }

    public static void setOtherData(ItemContainer container, TagCompound compound){
        container.internalOtherData = compound;
    }

    public static TagCompound readOtherData(ItemContainer container){
        return container.internalOtherData;
    }

    public static int readCount(ItemContainer container){
        return container.directCount();
    }
    public static TagCompound readData(ItemContainer container){
        return container.directData();
    }
}
