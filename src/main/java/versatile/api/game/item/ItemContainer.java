package versatile.api.game.item;

import versatile.api.game.OptionalItemInfo;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.block.data.DataCapacitor;
import versatile.api.game.block.data.TagCompound;
import versatile.api.internal.INItemUtil;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public final class ItemContainer implements DataCapacitor, Cloneable {
    public static final ItemContainer EMPTY = new ItemContainer(BlockInfo.EMPTY, 0);

    private PropertiedItem primaryItem;
    private int count;
    private TagCompound data;

    @Nullable
    private Set<PropertiedItem> itemVariations;


    public ItemContainer(PropertiedItem item, int count) {
        primaryItem = item;
        itemVariations = INItemUtil.getAliases(item);
        this.count = item.getInfo().equals(BlockInfo.EMPTY) ? 0 : count;
    }

    public ItemContainer(BlockInfo item, int count) {
        this(new PropertiedItem(item, INItemUtil.getDefaultProperty(item)), count);
    }

    public ItemContainer(OptionalItemInfo item, int count) {
        this(item.orEmpty(), count);
    }

    public ItemContainer(OptionalItemInfo item) {
        this(item.orEmpty(), 1);
    }

    public ItemContainer(PropertiedItem item) {
        this(item, 1);
    }

    public boolean hasProperty(VanillaItem.Property property) {
        for (PropertiedItem item : getItemVariations())
            if (item.getProperty().isPresent() && item.getProperty().get() == property)
                return true;
        return false;
    }

//    private void fillTag(VanillaItem.Property... properties) {
//        TagCompound tag = getData();
//        for (VanillaItem.Property property : properties) {
//            tag.set("v_internal_prop_" + property.getTagName(), property.getName());
//        }
//    }

    public boolean isEmpty() {
        return getCount() <= 0 || primaryItem.getInfo().isEmpty();
    }


    public Set<PropertiedItem> getItemVariations() {
        if (getCount() <= 0) //just in case
            return Collections.singleton(PropertiedItem.empty());
        return itemVariations != null ? itemVariations : Collections.singleton(primaryItem);
    }

    public boolean isItemEqual(BlockInfo info) {
        for (PropertiedItem p : getItemVariations())
            if (p.getInfo().equals(info))
                return true;
        return false;
    }

    public boolean isItemEqual(PropertiedItem item) {
        return getItemVariations().contains(item);
    }

    public boolean isItemEqual(OptionalItemInfo info) {
        return isItemEqual(info.orEmpty());
    }

    public boolean isItemEqual(ItemContainer item) {
        if (item.isEmpty())
            return isEmpty();
        return isItemEqual(item.primaryItem);
    }

    public int consume(int count) {
        int i = this.count -= Math.min(count, getCount());
        //internal api code
        if (internalCallback != null)
            internalCallback.onStackSizeChanged(this, this.count);
        return i;
    }

    /**
     * The item container personal tag with lazy initialization.
     * If this data tag is not empty as expected, then the data
     * in it could be added by the API itself, their key usually
     * starts with the string "v_internal_"
     *
     * @return non-null itemContainer's data tag, default is empty
     */
    public TagCompound getData() {
        //internal api code
        data = getActualTagOrNull();

        if (data == null) {
            data = TagCompound.create();
            //internal api code
            if (internalCallback != null)
                internalCallback.onDataTagChanged(this, data);
        }
        return data;
    }

    /**
     * @return true if itemContainer's data tag is empty
     */
    public boolean isDataEmpty() {
        TagCompound tag = getActualTagOrNull();
        return tag == null || tag.isEmpty();
    }

    public int increase(int count) {
        if (isEmpty())
            return 0;
        this.count = Math.max(0, count + getCount());
        //internal api code
        if (internalCallback != null)
            internalCallback.onStackSizeChanged(this, this.count);
        return this.count;
    }

    public int getCount() {
        //internal api code
        if (internalCallback != null)
            return count = internalCallback.getActualCount(this);
        return count;
    }

    @Override
    public void restoreDataFrom(TagCompound tag) {
        PropertiedItem lastType = primaryItem; //internal api code

        primaryItem = PropertiedItem.deserialize(tag.getTagOrEmpty("item"));
        count = tag.getIntOrDefault("count", 0);
        data = tag.getTagOrDefault("data", null);

        itemVariations = INItemUtil.getAliases(primaryItem);

        boolean itemChanged = !lastType.equals(primaryItem) && (itemVariations == null || !itemVariations.contains(lastType));

        //internal api code
        if (internalCallback != null) {
            if (itemChanged && !internalCallback.allowChangeItemType(this))
                throw new RuntimeException("Changing the itemContainer's type is not allowed");
            internalCallback.onStackSizeChanged(this, this.count);
            internalCallback.onDataTagChanged(this, data);
        }
    }

    @Override
    public String toString() {
        String tag = isDataEmpty() ? "" : ("$"  + getActualTagOrNull().toString());
        return primaryItem.toString() + "@" + getCount() + tag;
    }

    @Override
    public void saveDataTo(TagCompound tag) {
        tag.set("item", primaryItem.serialize());
        tag.set("count", getCount());
        TagCompound actual = getActualTagOrNull();
        if (actual != null && !actual.isEmpty())
            tag.set("data", actual);
    }

    public static ItemContainer deserialize(TagCompound tag) {
        ItemContainer container = new ItemContainer(BlockInfo.EMPTY, 1);
        container.restoreDataFrom(tag);
        return container;
    }

    @Override
    public ItemContainer clone() {
        ItemContainer clone = null;
        try {
            clone = (ItemContainer) super.clone();
            TagCompound actual = getActualTagOrNull();
            if (actual != null)
                clone.data = actual.clone();
            clone.primaryItem = primaryItem.clone();
            itemVariations = INItemUtil.getAliases(clone.primaryItem);

            clone.internalCallback = null; // internal api code
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemContainer container = (ItemContainer) o;
        return getCount() == container.getCount() &&
                areTagsEqual(container) &&
                Objects.equals(getItemVariations(), container.getItemVariations());
    }

    @Override
    public int hashCode() {
        TagCompound actualTagOrNull = getActualTagOrNull();
        int hash = 0;

        if (actualTagOrNull != null && !actualTagOrNull.isEmpty())
            hash = actualTagOrNull.hashCode();
        return Objects.hash(getCount(), hash, getItemVariations());
    }

    /*
     * Internal api code
     */

    @Nullable
    private TagCompound getActualTagOrNull() {
        if (internalCallback != null)
            return internalCallback.getActualTag(this, data);
        return data;
    }

    private boolean areTagsEqual(ItemContainer container) {
        TagCompound tag1 = getActualTagOrNull();
        TagCompound tag2 = container.getActualTagOrNull();

        boolean empty1 = tag1 == null || tag1.isEmpty();
        boolean empty2 = tag2 == null || tag2.isEmpty();


        return (empty1 && empty2) || Objects.equals(tag1, tag2);
    }

    INItemUtil.INItemCallback internalCallback;
    TagCompound internalOtherData;


    int directCount() {
        return count;
    }

    TagCompound directData() {
        return data;
    }
}
