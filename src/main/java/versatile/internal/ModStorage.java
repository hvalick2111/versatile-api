package versatile.internal;

import versatile.api.IMod;
import versatile.api.Modification;

public class ModStorage {
    private final Modification modAnnotation;
    private final IMod mod;

    public ModStorage(Modification modAnnotation, IMod mod) {
        this.modAnnotation = modAnnotation;
        this.mod = mod;
    }

    public String getModid() {
        return modAnnotation.value();
    }

    public IMod getMod() {
        return mod;
    }

    public Modification getAnnotation() {
        return modAnnotation;
    }
}
