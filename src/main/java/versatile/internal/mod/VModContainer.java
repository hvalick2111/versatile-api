package versatile.internal.mod;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.eventbus.EventBus;
import net.minecraftforge.fml.common.LoadController;
import net.minecraftforge.fml.common.MetadataCollection;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.ModMetadata;
import net.minecraftforge.fml.common.versioning.ArtifactVersion;
import net.minecraftforge.fml.common.versioning.VersionRange;
import versatile.api.IMod;
import versatile.api.mod.InfoEnvironment;
import versatile.api.mod.ModInfo;
import versatile.internal.lang.LangDispatcher;

import javax.annotation.Nullable;
import java.io.File;
import java.net.URL;
import java.security.cert.Certificate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class VModContainer implements ModContainer {

    private ModInfo info;
    private String modid;
    private ModContainer versatileContainer;

    public VModContainer(String modid, IMod mod, InfoEnvironment environment, ModContainer versatileContainer) {
        this.info = mod.createModInfo(environment);
        this.modid = modid;
        this.versatileContainer = versatileContainer;
    }

    @Override
    public String getModId() {
        return modid;
    }

    @Override
    public String getName() {
        return info.name();
    }

    @Override
    public String getVersion() {
        return info.version();
    }

    @Override
    public File getSource() {
        return versatileContainer.getSource();
    }

    @Override
    public ModMetadata getMetadata() {
        ModMetadata metadata = new ModMetadata();
        metadata.description = info.description().deduceName(LangDispatcher.instance());
        metadata.authorList.addAll(Arrays.asList(info.authors()));
        return metadata;
    }

    @Override
    public void bindMetadata(MetadataCollection mc) {

    }

    @Override
    public void setEnabledState(boolean enabled) {

    }

    @Override
    public Set<ArtifactVersion> getRequirements() {
        return ImmutableSet.of();
    }

    @Override
    public List<ArtifactVersion> getDependencies() {
        return ImmutableList.of();
    }

    @Override
    public List<ArtifactVersion> getDependants() {
        return ImmutableList.of();
    }

    @Override
    public String getSortingRules() {
        return null;
    }

    @Override
    public boolean registerBus(EventBus bus, LoadController controller) {
        return false;
    }

    @Override
    public boolean matches(Object mod) {
        return false;
    }

    @Override
    public Object getMod() {
        return null;
    }

    @Override
    public ArtifactVersion getProcessedVersion() {
        return null;
    }

    @Override
    public boolean isImmutable() {
        return false;
    }

    @Override
    public String getDisplayVersion() {
        return info.version();
    }

    @Override
    public VersionRange acceptableMinecraftVersionRange() {
        return null;
    }

    @Nullable
    @Override
    public Certificate getSigningCertificate() {
        return null;
    }

    @Override
    public Map<String, String> getCustomModProperties() {
        return null;
    }

    @Override
    public Class<?> getCustomResourcePackClass() {
        return null;
    }

    @Override
    public Map<String, String> getSharedModDescriptor() {
        return null;
    }

    @Override
    public Disableable canBeDisabled() {
        return Disableable.NEVER;
    }

    @Override
    public String getGuiClassName() {
        return null;
    }

    @Override
    public List<String> getOwnedPackages() {
        return null;
    }

    @Override
    public boolean shouldLoadInEnvironment() {
        return false;
    }

    @Override
    public URL getUpdateUrl() {
        return null;
    }

    @Override
    public void setClassVersion(int classVersion) {

    }

    @Override
    public int getClassVersion() {
        return 0;
    }
}
