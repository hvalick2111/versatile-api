package versatile.internal.mod;

import versatile.api.game.lang.IDisplayName;
import versatile.api.mod.InfoEnvironment;
import versatile.api.mod.ModInfo;

public class InfoDispatcher implements InfoEnvironment {
    @Override
    public ModInfo createInfo(String name) {
        return new BasicModInfo(name, "1.0", new String[]{}, lang -> lang.permanent(""));
    }

    @Override
    public ModInfo createInfo(String name, String version) {
        return new BasicModInfo(name, version, new String[]{}, lang -> lang.permanent(""));
    }

    @Override
    public ModInfo createInfo(String name, String version, String description, String... authors) {
        return new BasicModInfo(name, version, new String[]{}, lang -> lang.permanent(description));
    }

    @Override
    public ModInfo createInfo(String name, String version, IDisplayName description, String... authors) {
        return new BasicModInfo(name, version, new String[]{}, description);
    }
}
