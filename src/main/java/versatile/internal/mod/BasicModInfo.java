package versatile.internal.mod;

import versatile.api.game.lang.IDisplayName;
import versatile.api.mod.ModInfo;

public class BasicModInfo implements ModInfo {
    private String name;
    private String version;
    private String[] authors;
    private IDisplayName description;

    public BasicModInfo(String name, String version, String[] authors, IDisplayName description) {
        this.name = name;
        this.version = version;
        this.authors = authors;
        this.description = description;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String version() {
        return version;
    }

    @Override
    public String[] authors() {
        return authors;
    }

    @Override
    public IDisplayName description() {
        return description;
    }
}
