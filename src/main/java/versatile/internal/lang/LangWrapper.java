package versatile.internal.lang;

import versatile.api.game.lang.LangManager;

public class LangWrapper implements LangManager {
    private static LangWrapper instance = new LangWrapper();

    public static LangWrapper getInstance() {
        return instance;
    }

    @Override
    public String fromLangProperties(String key) {
        return key;
    }

    @Override
    public String permanent(String constantName) {
        return constantName;
    }
}
