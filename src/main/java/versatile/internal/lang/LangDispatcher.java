package versatile.internal.lang;

import net.minecraft.client.resources.IResource;
import versatile.api.game.lang.LangManager;
import versatile.internal.mc.McLang;
import versatile.internal.mc.ModResLocation;
import versatile.internal.mc.resource.McResLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Properties;

public class LangDispatcher implements LangManager {
    private static final LangDispatcher instance = new LangDispatcher();
    private final HashMap<String, Properties> langProp = new HashMap<>();

    public static LangDispatcher instance() {
        return instance;
    }

    public void load(Iterable<String> modIds) {
        for (String modid : modIds) {
            loadLang(modid, "ru");
            loadLang(modid, "en");
        }
    }

    private void loadLang(String modid, String lang) {
        String path = "lang/" + lang + ".properties";
        ModResLocation location = new ModResLocation(modid, path);
        IResource resource = McResLoader.getResource(location);

        if(resource == null) return;
        InputStream stream = resource.getInputStream();

        try (Reader isr = new InputStreamReader(stream, StandardCharsets.UTF_8)) {
            Properties properties = new Properties();
            properties.load(isr);

            Properties existed = langProp.get(lang);
            if (existed == null)
                langProp.put(lang, properties);
            else
                existed.putAll(properties);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String fromLangProperties(String key) {
        String domain = McLang.getCurrentDomain();
        Properties properties = langProp.get(domain);
        if(properties == null) return key;

        Object o = properties.get(key);
        return o == null ? key : o.toString();
    }

    @Override
    public String permanent(String constantName) {
        return constantName;
    }
}
