package versatile.internal;

import java.lang.reflect.Method;

@FunctionalInterface
public interface FunctionMethod<T, R extends Method> {

    /**
     * Applies this function to the given argument.
     *
     * @param t the function argument
     * @return the function result
     */
    R apply(T t) throws NoSuchMethodException;
}

