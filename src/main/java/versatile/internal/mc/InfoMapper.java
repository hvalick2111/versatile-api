package versatile.internal.mc;

import net.minecraft.util.ResourceLocation;
import versatile.api.game.block.BlockInfo;

public class InfoMapper {
    public static BlockInfo map(ResourceLocation info){
        if(info == null) return BlockInfo.EMPTY;
        return new BlockInfo(info.getResourceDomain(), info.getResourcePath());
    }

    public static ResourceLocation map(BlockInfo info){
        return new ResourceLocation(info.getOwner(), info.getName());
    }
}
