package versatile.internal.mc.entity;


import net.minecraft.entity.Entity;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import versatile.api.game.entity.EntityHandler;
import versatile.api.game.world.WorldController;
import versatile.internal.mc.world.WorldMapper;

import java.util.List;

public class EntityHandlers {

    @SuppressWarnings("all") //for stupid generics
    @SubscribeEvent(receiveCanceled = true, priority = EventPriority.HIGH)
    public void activated(EntityJoinWorldEvent event) {
        Entity entity = event.getEntity();
        List<EntityHandler<?>> handlers = EntityCache.getHandlers(entity);
        if(handlers == null) return;

        boolean isCanceled = false;
        for(EntityHandler handler : handlers){
            WorldController world = WorldMapper.get(event.getWorld());
            if(handler.entityJoinWorld(world, InternalEntityController.get(entity)))
                isCanceled = true;
        }
        event.setCanceled(isCanceled);
    }

}
