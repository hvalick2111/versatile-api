package versatile.internal.mc.entity.world;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.EntityEntry;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.block.data.TagCompound;
import versatile.api.game.entity.EntityContainer;
import versatile.api.game.world.Pos3d;
import versatile.internal.mc.InfoMapper;
import versatile.internal.mc.data.InternalTagCompound;

public class InternalEntityContainer <T extends Entity> implements EntityContainer {
    protected T contained;

    public InternalEntityContainer(T contained) {
        this.contained = contained;
    }

    @Override
    public BlockInfo getType() {
        return InfoMapper.map(EntityList.getKey(contained));
    }

    @Override
    public Pos3d getPosition() {
        return new Pos3d(contained.posX, contained.posY, contained.posZ);
    }

    @Override
    public void setPosition(Pos3d pos) {
        contained.setPositionAndUpdate(pos.x(), pos.y(), pos.z());
    }

    @SuppressWarnings("unchecked")
    @Override
    public void restoreDataFrom(TagCompound tag) {
        NBTTagCompound nbt = ((InternalTagCompound) tag).copyCompound();
        ResourceLocation resourcelocation = new ResourceLocation(nbt.getString("id"));
        EntityEntry value = ForgeRegistries.ENTITIES.getValue(resourcelocation);
        if(value.getEntityClass() != contained.getClass()){
            contained.setDead();
            contained = (T) EntityList.createEntityByIDFromName(resourcelocation, contained.world);
        } else {
            contained.readFromNBT(nbt);
        }
    }

    @Override
    public void saveDataTo(TagCompound tag) {
        NBTTagCompound compound = new NBTTagCompound();
        contained.writeToNBTOptional(compound);
        tag.extractFrom(new InternalTagCompound(compound));
    }
}
