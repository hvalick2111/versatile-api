package versatile.internal.mc.entity.world;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Container;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.event.entity.player.PlayerContainerEvent;
import versatile.api.game.GameSide;
import versatile.api.game.entity.Hand;
import versatile.api.game.entity.world.PlayerContainer;
import versatile.api.game.gui.GuiInventory;
import versatile.api.game.inventory.Inventory;
import versatile.api.game.item.ItemContainer;
import versatile.internal.mc.gui.container.ContainerInventory;
import versatile.internal.mc.item.ItemMapper;
import versatile.internal.mc.packet.PacketRegister;
import versatile.internal.mc.packet.messages.PacketOpenInventory;

import java.util.function.Consumer;

public class InternalPlayerContainer extends InternalEntityContainer<EntityPlayer> implements PlayerContainer {
    public InternalPlayerContainer(EntityPlayer contained) {
        super(contained);
    }

    @Override
    public ItemContainer getHeldItem(Hand hand) {
        return ItemMapper.wrap(contained.getHeldItem(map(hand)));
    }

    @Override
    public void setHeldItem(Hand hand, ItemContainer newHeldItem) {
        contained.setHeldItem(map(hand), ItemMapper.unwrap(newHeldItem));
    }

    @Override
    public void openInventory(Inventory inventory, Consumer<GuiInventory> setting, GameSide effective) {
        GuiInventory gui = new GuiInventory(inventory);
        open(inventory, gui, setting, effective);
    }

    private void open(Inventory inventory, GuiInventory gui, Consumer<GuiInventory> setting, GameSide effective) {
        World world = contained.world;
        if (!world.isRemote) {
            if (!(contained instanceof FakePlayer)) {
                EntityPlayerMP player = (EntityPlayerMP) contained;
                setting.accept(gui);
                Container remoteGuiContainer = new ContainerInventory(contained, gui);
                if (remoteGuiContainer != null) {
                    player.getNextWindowId();
                    player.closeContainer();
                    int windowId = player.currentWindowId;
                    PacketRegister.NETWORK.sendTo(new PacketOpenInventory(windowId, inventory.size(), gui, effective), player);

                    player.openContainer = remoteGuiContainer;
                    player.openContainer.windowId = windowId;
                    player.openContainer.addListener(player);
                    MinecraftForge.EVENT_BUS.post(new PlayerContainerEvent.Open(contained, contained.openContainer));
                }
            }
        } else if (effective == GameSide.CLIENT) {
            setting.accept(gui);
            PacketOpenInventory.setLastClientGui(gui);
        }
    }

    public static EnumHand map(Hand hand) {
        return hand == Hand.MAIN ? EnumHand.MAIN_HAND : EnumHand.OFF_HAND;
    }

    public static Hand mapR(EnumHand hand) {
        return hand == EnumHand.MAIN_HAND ? Hand.MAIN : Hand.OFF;
    }
}
