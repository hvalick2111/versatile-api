package versatile.internal.mc.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import versatile.internal.TODO;
import versatile.internal.mc.entity.world.InternalEntityContainer;
import versatile.internal.mc.entity.world.InternalPlayerContainer;

import java.util.WeakHashMap;

public class InternalEntityController {
    private static WeakHashMap<Entity, InternalEntityContainer<?>> controllerMap = new WeakHashMap<>();

    public static InternalEntityContainer<?> get(Entity entity) {
        return createEntity(entity);
    }

    private static InternalEntityContainer<?> createEntity(Entity entity) {
        if (entity instanceof EntityPlayer)
            return new InternalPlayerContainer((EntityPlayer) entity);
        else throw new TODO();
//        return new InternalEntityContainer<>(entity);
    }
}
