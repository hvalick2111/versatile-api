package versatile.internal.mc.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import versatile.api.game.block.BlockInfo;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public enum InternalEntity {
    PLAYER(new BlockInfo("minecraft", "player"), EntityPlayer.class),
    ZOMBIE(new BlockInfo("minecraft", "zombie"), EntityZombie.class),
    ;

    private BlockInfo info;
    private Class<? extends Entity> entityClass;
    private static Map<BlockInfo, InternalEntity> entityMap = null;

    InternalEntity(BlockInfo info, Class<? extends Entity> entityClass) {
        this.info = info;
        this.entityClass = entityClass;
    }

    public BlockInfo getInfo() {
        return info;
    }

    public UUID summon(World world, double x, double y, double z){
        if(this == PLAYER) throw new RuntimeException("Player can't be summoned");
        try {
            Entity entity = entityClass.getConstructor(World.class).newInstance(world);
            entity.setPosition(x, y, z);
            world.spawnEntity(entity);
            return entity.getPersistentID();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static UUID summonEntity(World world, Entity entity, double x, double y, double z){
        try {
            entity.setPosition(x, y, z);
            world.spawnEntity(entity);
            return entity.getPersistentID();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static InternalEntity find(BlockInfo info){
        if(entityMap == null){
            entityMap = new HashMap<>();
            for(InternalEntity entity: values()){
                entityMap.put(entity.info, entity);
            }
        }
        return entityMap.get(info);
    }
}
