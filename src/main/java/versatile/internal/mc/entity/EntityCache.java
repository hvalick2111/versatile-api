package versatile.internal.mc.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.EntityEntry;
import net.minecraftforge.registries.GameData;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.entity.EntityHandler;
import versatile.internal.mc.InfoMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EntityCache {
    private static Map<Class<? extends Entity>, List<EntityHandler<?>>> backedHandlers = new HashMap<>();
    private static Map<Class<? extends Entity>, BlockInfo> backedEntities = new HashMap<>();
    private static List<EntityHandler<?>> playersHandlers = new ArrayList<>();

    public static void bake(Map<BlockInfo, List<EntityHandler<?>>> raw){
        Map<Class<? extends Entity>, EntityEntry> mapper = GameData.getEntityClassMap();
        for(Map.Entry<BlockInfo, List<EntityHandler<?>>> entry : raw.entrySet()){
            ResourceLocation location = InfoMapper.map(entry.getKey());
            if(location.toString().equals("minecraft:player")){
                playersHandlers.addAll(entry.getValue());
                continue;
            }
            EntityEntry value = GameData.getEntityRegistry().getValue(location);
            Class<? extends Entity> entityClass = value.getEntityClass();
            backedHandlers.put(entityClass, entry.getValue());
            backedEntities.put(entityClass, entry.getKey());
        }
    }

    public static BlockInfo getInfo(Entity entity){
        return backedEntities.get(entity.getClass());
    }

    public static List<EntityHandler<?>> getHandlers(Entity entity){
        if(entity instanceof EntityPlayer) return playersHandlers;
        return backedHandlers.get(entity.getClass());
    }
}
