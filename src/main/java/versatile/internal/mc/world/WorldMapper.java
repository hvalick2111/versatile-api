package versatile.internal.mc.world;

import net.minecraft.world.World;
import versatile.api.game.world.WorldController;

import java.util.HashMap;
import java.util.Map;

public class WorldMapper {
    private static Map<World, WorldController> worlds = new HashMap<>();

    public static WorldController get(World key) {
        return worlds.computeIfAbsent(key, InternalWorldWrapper::new);
    }

}
