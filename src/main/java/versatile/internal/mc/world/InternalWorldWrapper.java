package versatile.internal.mc.world;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import versatile.api.game.GameSide;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.block.data.DataCapacitor;
import versatile.api.game.entity.EntityContainer;
import versatile.api.game.item.ItemContainer;
import versatile.api.game.world.Pos;
import versatile.api.game.world.Pos3d;
import versatile.api.game.world.WorldController;
import versatile.internal.mc.block.BlockCache;
import versatile.internal.mc.block.tile.TileEntityDataBlock;
import versatile.internal.mc.entity.InternalEntity;
import versatile.internal.mc.item.ItemMapper;

import java.util.UUID;

public class InternalWorldWrapper implements WorldController {
    private World world;

    public InternalWorldWrapper(World world) {
        this.world = world;
    }

    @Override
    public void setBlock(BlockInfo block, Pos pos) {
        if (!side().isClient()) {
            BlockPos remapped = remapPos(pos);
            if(block == BlockInfo.EMPTY)
                world.setBlockState(remapped, Blocks.AIR.getDefaultState());
            else
                world.setBlockState(remapped, BlockCache.getBlock(block).getDefaultState());
        }
    }

    @Override
    public BlockInfo getBlock(Pos pos) {
        return BlockCache.get(world.getBlockState(remapPos(pos)).getBlock());
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends DataCapacitor> T getBlockData(Pos pos) {
        TileEntity tileEntity = world.getTileEntity(remapPos(pos));
        if(tileEntity instanceof TileEntityDataBlock)
            return (T) (side().isClient() ? ((TileEntityDataBlock) tileEntity).getAllocator().get() : ((TileEntityDataBlock) tileEntity).getCapacitor());
        return null;
    }

    private static BlockPos remapPos(Pos pos){
        return new BlockPos(pos.x(), pos.y(), pos.z());
    }

    private static GameSide sideOf(World world){
        return world.isRemote ? GameSide.CLIENT : GameSide.SERVER;
    }

    @Override
    public GameSide side() {
        return sideOf(world);
    }

    @Override
    public UUID spawnEntity(EntityContainer object) {
        if (!side().isClient()) {
            Pos3d pos3d = object.getPosition();
            InternalEntity entity = InternalEntity.find(object.getType());
            if (entity != null) return entity.summon(world, pos3d.x(), pos3d.y(), pos3d.z());
        }
        return null;
    }

    @Override
    public UUID spawnEntity(ItemContainer object, Pos3d pos) {
        if (!side().isClient()) {
            EntityItem entity = new EntityItem(world);
            entity.setItem(ItemMapper.unwrap(object));
            entity.setPosition(pos.x(), pos.y(), pos.z());
            world.spawnEntity(entity);
            return entity.getPersistentID();
        }
        return null;
    }

    @Override
    public EntityContainer getEntity(UUID uuid) {
        return null;
    }

    @Override
    public EntityContainer[] getAllEntities(double radius) {
        return null;
    }
}
