package versatile.internal.mc;

import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class McLang {
    public static String getCurrentDomain() {
        if (FMLCommonHandler.instance().getSide().isClient())
            return Minecraft.getMinecraft()
                    .getLanguageManager()
                    .getCurrentLanguage()
                    .getLanguageCode().split("\\_")[0];
        return "en";
    }
}
