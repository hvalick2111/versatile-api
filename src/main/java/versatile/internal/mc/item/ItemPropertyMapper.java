package versatile.internal.mc.item;

import com.google.common.collect.HashBiMap;
import com.google.common.collect.Sets;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Tuple;
import versatile.api.game.item.ItemContainer;
import versatile.api.game.item.PropertiedItem;
import versatile.api.game.item.VanillaItem;
import versatile.api.game.item.VanillaItem.Property;
import versatile.api.game.item.property.ColorType;
import versatile.api.game.item.property.WoodType;
import versatile.internal.mc.Pair;

import javax.annotation.Nullable;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ItemPropertyMapper{
    private static final String PROP_PREFIX = "v_internal_prop_";



    private static HashBiMap<ItemPattern, Pair<ItemPattern, Property>> propertyMap = HashBiMap.create();
    private static List<Pair<String, Function<String, ? extends Property>>> deserializerPropList = new ArrayList<>();
    private static HashMap<PropertiedItem, HashSet<PropertiedItem>> propertiedAliases = new HashMap<>();

    private static void addAliases(HashSet<PropertiedItem> items){
        for (PropertiedItem item: items)
            propertiedAliases.put(item, items);
    }

    @Nullable
    public static Set<PropertiedItem> getAliases(PropertiedItem item){
        return propertiedAliases.get(item);
    }

    public static Set<PropertiedItem> getAliasesWithoutNull(PropertiedItem item){
        return propertiedAliases.getOrDefault(item, Sets.newHashSet(item));
    }

    static {
        registerProperty(WoodType.tagName, WoodType::typeOrDefault);
        registerProperty(ColorType.tagName, ColorType::typeOrDefault);
    }

    private static <T extends Property> void registerProperty(String tagName, Function<String, T> function){
        deserializerPropList.add(new Pair<>(tagName,function));
    }

    private static ItemPattern of(Item i){
        return new ItemPattern(i, 0);
    }

    private static ItemPattern of(Item i, int meta){
        return new ItemPattern(i, meta);
    }

    private static List<Property> deserialize(ItemContainer container){
        if(container.isDataEmpty()) return new ArrayList<>();

        List<Property> list = new ArrayList<>();
        for (Pair<String, Function<String, ? extends Property>> f : deserializerPropList){
            String key = PROP_PREFIX + f.getFirst();
            if(container.getData().contains(key)){
                String propValue = container.getData().getStringOrDefault(key, (String) null);
                if(propValue == null) throw new RuntimeException("Unexpected behavior");
                list.add(f.getSecond().apply(propValue));
            }
        }


        return list;
    }

//    public static ItemPattern fromPropertied(PropertiedItem propertiedItem, int oldMeta){
//        Pair<ItemPattern, HashSet<PropertiedItem>> pair = propertiedAliases.get(propertiedItem);
//        if(pair == null){
//            BlockInfo info = propertiedItem.getInfo();
//            Item item = Item.REGISTRY.getObject(new ResourceLocation(info.getOwner(), info.getName()));
//            return new ItemPattern(item, oldMeta);
//        }
//        return pair.getFirst();
//    }

    public static Tuple<ItemPattern, List<Property>> of(ItemStack item){
        Pair<ItemPattern, Property> newItem = propertyMap.get(of(item.getItem(), item.getMetadata()));

        if(newItem == null) return new Tuple<>(of(item.getItem(), item.getMetadata()), Collections.emptyList());

        List<Property> properties = new ArrayList<>();
        properties.add(newItem.getSecond());
        return new Tuple<>(newItem.getFirst(), properties.stream().filter(Objects::nonNull).collect(Collectors.toList()));
    }

    public static ItemPattern of(PropertiedItem container, int meta){

        ItemPattern res = null;
        for (PropertiedItem prop : getAliasesWithoutNull(container)) {
            ResourceLocation name = new ResourceLocation(prop.getInfo().getOwner(), prop.getInfo().getName());
            Item u = Item.REGISTRY.getObject(name);
            ItemPattern primary = propertyMap.inverse().get(new Pair<>(of(u, meta), prop.getProperty().orElse(null)));
            if(primary == null) continue;
            res = primary;
        }

        if(res == null) {
            ResourceLocation name = new ResourceLocation(container.getInfo().getOwner(), container.getInfo().getName());
            Item u = Item.REGISTRY.getObject(name);
            return of(u, meta);
        }
        return res;
    }

    public static HashBiMap<ItemPattern, Pair<ItemPattern, Property>> getPropertyMap() {
        return propertyMap;
    }

    public static void init(){
        propertyMap.put(of(Items.BOAT), new Pair<>(of(Items.BOAT), WoodType.OAK));
        propertyMap.put(of(Items.BIRCH_BOAT), new Pair<>(of(Items.BOAT), WoodType.BIRCH));
        propertyMap.put(of(Items.SPRUCE_BOAT), new Pair<>(of(Items.BOAT), WoodType.SPRUCE));
        propertyMap.put(of(Items.JUNGLE_BOAT), new Pair<>(of(Items.BOAT), WoodType.JUNGLE));
        propertyMap.put(of(Items.ACACIA_BOAT), new Pair<>(of(Items.BOAT), WoodType.ACACIA));
        propertyMap.put(of(Items.DARK_OAK_BOAT), new Pair<>(of(Items.BOAT), WoodType.DARK_OAK));


        propertyMap.put(of(Items.DYE,  0), new Pair<>(of(Items.DYE, 0), ColorType.BLACK));
        propertyMap.put(of(Items.DYE,  1), new Pair<>(of(Items.DYE, 0), ColorType.RED));
        propertyMap.put(of(Items.DYE,  2), new Pair<>(of(Items.DYE, 0), ColorType.GREEN));
        propertyMap.put(of(Items.DYE,  3), new Pair<>(of(Items.DYE, 0), ColorType.BROWN));
        propertyMap.put(of(Items.DYE,  4), new Pair<>(of(Items.DYE, 0), ColorType.BLUE));
        propertyMap.put(of(Items.DYE,  5), new Pair<>(of(Items.DYE, 0), ColorType.PURPLE));
        propertyMap.put(of(Items.DYE,  6), new Pair<>(of(Items.DYE, 0), ColorType.CYAN));
        propertyMap.put(of(Items.DYE,  7), new Pair<>(of(Items.DYE, 0), ColorType.LIGHT_GRAY));
        propertyMap.put(of(Items.DYE,  8), new Pair<>(of(Items.DYE, 0), ColorType.GRAY));
        propertyMap.put(of(Items.DYE,  9), new Pair<>(of(Items.DYE, 0), ColorType.PINK));
        propertyMap.put(of(Items.DYE, 10), new Pair<>(of(Items.DYE, 0), ColorType.LIME));
        propertyMap.put(of(Items.DYE, 11), new Pair<>(of(Items.DYE, 0), ColorType.YELLOW));
        propertyMap.put(of(Items.DYE, 12), new Pair<>(of(Items.DYE, 0), ColorType.LIGHT_BLUE));
        propertyMap.put(of(Items.DYE, 13), new Pair<>(of(Items.DYE, 0), ColorType.MAGENTA));
        propertyMap.put(of(Items.DYE, 14), new Pair<>(of(Items.DYE, 0), ColorType.ORANGE));
        propertyMap.put(of(Items.DYE, 15), new Pair<>(of(Items.DYE, 0), ColorType.WHITE));




//        addAliases(new ItemPattern(Items.DYE, 8), ImmutableSet.of(new PropertiedItem(of)));
    }

    public static void initAliases(){
        addAliases(Sets.newHashSet(VanillaItem.DYE.with(ColorType.WHITE), new PropertiedItem(VanillaItem.BONE_MEAL.orEmpty())));
        addAliases(Sets.newHashSet(VanillaItem.DYE.with(ColorType.BLACK), new PropertiedItem(VanillaItem.INK_SAC.orEmpty())));
        addAliases(Sets.newHashSet(VanillaItem.DYE.with(ColorType.BROWN), new PropertiedItem(VanillaItem.COCOA_BEANS.orEmpty())));
        addAliases(Sets.newHashSet(VanillaItem.DYE.with(ColorType.BLUE), new PropertiedItem(VanillaItem.LAPIS_LAZULI.orEmpty())));
    }

    /*

        Items.BOAT -> OAK, BOAT
        Items.BIRCH_BOAT -> BIRCH, BOAT

        DualHashMap<ItemPattern, Pair<Property, ItemPattern>>

     */


}
