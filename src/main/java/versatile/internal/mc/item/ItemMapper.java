package versatile.internal.mc.item;

import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Tuple;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.block.data.TagCompound;
import versatile.api.game.item.ItemContainer;
import versatile.api.game.item.PropertiedItem;
import versatile.api.game.item.VanillaItem;
import versatile.api.game.item._INItemWrapper;
import versatile.api.internal.INItemUtil;
import versatile.internal.mc.data.InternalTagCompound;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Set;

import static versatile.api.internal.INItemUtil.linkItemToCallback;

public class ItemMapper {



    public static ItemContainer wrap(ItemStack stack){
        Tuple<ItemPattern, List<VanillaItem.Property>> tuple = ItemPropertyMapper.of(stack);
        ResourceLocation location = tuple.getFirst().getItem().delegate.name();
        BlockInfo item = new BlockInfo(location.getResourceDomain(), location.getResourcePath());

        VanillaItem.Property property = null;
        List<VanillaItem.Property> propertyList = tuple.getSecond();
        if(propertyList.size() > 0)
            property = propertyList.get(0);

        final ItemContainer container = new ItemContainer(new PropertiedItem(item, property), stack.getCount());

        if(tuple.getFirst().getMeta() != 0){
            TagCompound compound = TagCompound.create();
            compound.set("v_internal_meta", tuple.getFirst().getMeta());
            _INItemWrapper.setOtherData(container, compound);
        }
        if(stack.hasTagCompound()){
            InternalTagCompound compound = new InternalTagCompound(stack.getTagCompound());
            container.getData().extractFrom(compound);
        }
        linkItems(stack, container);
        return container;
    }

    private static void linkItems(ItemStack stack, ItemContainer container) {
        WeakReference<ItemStack> weak = new WeakReference<>(stack);
        linkItemToCallback(container, new MapCallback(weak));
    }

    private static class MapCallback implements INItemUtil.INItemCallback {
        private WeakReference<ItemStack> weak;

        public MapCallback(WeakReference<ItemStack> weak) {
            this.weak = weak;
        }

        private void unlinkCallback(ItemContainer owner){
            _INItemWrapper.setCallBack(owner, null);
        }

        @Override
        public void onStackSizeChanged(ItemContainer container, int count) {
            ItemStack stack = weak.get();
            if(stack == null) {
                unlinkCallback(container);
                return;
            }

            stack.setCount(count);
        }

        @Override
        public void onDataTagChanged(ItemContainer container, TagCompound data) {
            ItemStack stack = weak.get();
            if(stack == null) {
                unlinkCallback(container);
                return;
            }

            if (data == null)
                stack.setTagCompound(null);
            else
                stack.setTagCompound(((InternalTagCompound) data).getDirectCompound());
        }

        @Override
        public int getActualCount(ItemContainer container) {
            ItemStack stack = weak.get();
            if(stack == null) {
                unlinkCallback(container);
                return _INItemWrapper.readCount(container);
            }
            return stack.getCount();
        }

        @Override
        public TagCompound getActualTag(ItemContainer container, TagCompound data) {
            ItemStack stack = weak.get();
            if(stack == null) {
                unlinkCallback(container);
                return _INItemWrapper.readData(container);
            }

            if (!stack.hasTagCompound())
                return null;
            if (stack != null && ((InternalTagCompound) data).getDirectCompound() == stack.getTagCompound())
                return data;
            return new InternalTagCompound(stack.getTagCompound(), false);
        }
    }

    /*
    public static ItemStack unwrap(ItemContainer container){
        ResourceLocation name = new ResourceLocation(container.getItemVariations().getOwner(), container.getItemVariations().getName());
        TagCompound otherData = _INItemWrapper.readOtherData(container);
        int oldMeta = otherData == null ? 0 : otherData.getIntOrDefault("v_internal_meta", 0);

        ItemPattern pattern = ItemPropertyMapper.of(Item.REGISTRY.getObject(name), container, oldMeta);
        ItemStack stack = new ItemStack(pattern.getItem(), container.getCount(), pattern.getMeta());

     */

    public static ItemStack unwrap(ItemContainer container){
        INItemUtil.INItemCallback contained = _INItemWrapper.readCallBack(container);
        if(contained != null){
            ItemStack stack = ((MapCallback) contained).weak.get();
            if(stack != null)
                return stack;
        }
        Set<PropertiedItem> itemVariations = container.getItemVariations();
        PropertiedItem propertiedItem = itemVariations.iterator().next();

        TagCompound otherData = _INItemWrapper.readOtherData(container);
        int oldMeta = otherData == null ? 0 : otherData.getIntOrDefault("v_internal_meta", 0);

        ItemPattern newPattern = ItemPropertyMapper.of(propertiedItem, oldMeta);
        ItemStack stack = new ItemStack(newPattern.getItem(), container.getCount(), newPattern.getMeta());

        TagCompound data = container.getData();
        if(!data.isEmpty())
            stack.setTagCompound(((InternalTagCompound)data).getDirectCompound());

        linkItems(stack, container);
        return stack;
    }
}
