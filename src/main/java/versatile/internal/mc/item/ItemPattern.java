package versatile.internal.mc.item;

import net.minecraft.item.Item;

import java.util.Objects;

public class ItemPattern {
    private Item item;
    private int meta;

    public ItemPattern(Item item, int meta) {
        this.item = item;
        this.meta = meta;
    }


    public Item getItem() {
        return item;
    }

    public int getMeta() {
        return meta;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemPattern that = (ItemPattern) o;
        return meta == that.meta &&
                item.equals(that.item);
    }

    @Override
    public int hashCode() {
        return Objects.hash(item, meta);
    }
}
