package versatile.internal.mc.item;

import com.google.common.collect.HashBiMap;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import versatile.api.game.block.BlockInfo;

public class ItemCache {
    private static HashBiMap<Item, BlockInfo> itemInfoMap = HashBiMap.create(2000);


    static {
        init();
    }

    @SuppressWarnings("all")
    private static void init(){
        itemInfoMap.inverse();
        itemInfoMap.get(Items.STICK);
    }


    public static Item getItem(BlockInfo info){
        return itemInfoMap.inverse().computeIfAbsent(info, in -> Item.REGISTRY.getObject(new ResourceLocation(info.getOwner(), info.getName())));
    }

    public static BlockInfo get(Item item){
        return itemInfoMap.computeIfAbsent(item, ItemCache::getBlockInfo);
    }

    public static BlockInfo add(Item item, BlockInfo blockInfo){
        itemInfoMap.put(item, blockInfo);
        return blockInfo;
    }

    private static BlockInfo getBlockInfo(Item item) {
        if(item == Items.AIR) return BlockInfo.EMPTY;
        ResourceLocation registryName = item.getRegistryName();
        return new BlockInfo(registryName.getResourceDomain(), registryName.getResourcePath());
    }
}
