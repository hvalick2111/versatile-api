package versatile.internal.mc.item;

import versatile.api.game.block.BlockInfo;
import versatile.api.game.item.IItem;
import versatile.api.game.item.ItemFactory;
import versatile.api.game.lang.IDisplayName;
import versatile.api.resource.texture.TextureFactory;

public class ItemInternalFactory implements ItemFactory, IItem {
    private final BlockInfo source;
    private IDisplayName displayName;
    private TextureFactory texture;

    public ItemInternalFactory(BlockInfo source) {
        this.source = source;
    }

    @Override
    public BlockInfo getSource() {
        return source;
    }

    @Override
    public void setDisplayName(IDisplayName displayName) {
        this.displayName = displayName;
    }

    @Override
    public void setTexture(TextureFactory texture) {
        this.texture = texture;
    }

    @Override
    public IDisplayName displayName() {
        return displayName;
    }

    @Override
    public TextureFactory texture() {
        return texture;
    }

}