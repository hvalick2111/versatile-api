package versatile.internal.mc.item;

import net.minecraft.item.Item;

import java.util.ArrayList;

public class ItemList <T extends Item & IInternalItem>
        extends ArrayList<T> {

    @SuppressWarnings("unchecked")
    public void add(ItemInternalWrapper wrapper) {
        super.add((T) wrapper);
    }

    @SuppressWarnings("unchecked")
    public void add(ItemBlockInternalWrapper wrapper) {
        super.add((T) wrapper);
    }
}