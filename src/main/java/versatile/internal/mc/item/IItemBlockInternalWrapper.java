package versatile.internal.mc.item;

import versatile.api.game.block.BlockInfo;
import versatile.api.game.block.IBlock;
import versatile.api.game.item.IItem;
import versatile.api.game.lang.IDisplayName;
import versatile.api.resource.texture.TextureFactory;

public class IItemBlockInternalWrapper implements IItem {
    private final BlockInfo source;
    private IDisplayName displayName;
    private TextureFactory textureFactory;

    public IItemBlockInternalWrapper(IBlock block) {
        source = block.getSource();
        displayName = block.getDisplayName();
        textureFactory = block.getTexture();
    }

    @Override
    public BlockInfo getSource() {
        return source;
    }

    @Override
    public IDisplayName displayName() {
        return displayName;
    }

    @Override
    public TextureFactory texture() {
        return textureFactory;
    }
}
