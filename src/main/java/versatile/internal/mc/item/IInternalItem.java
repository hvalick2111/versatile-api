package versatile.internal.mc.item;

import versatile.api.game.item.IItem;
import versatile.internal.mc.registry.IUnique;

import java.util.function.Supplier;

public interface IInternalItem extends Supplier<IItem>, IUnique {

    default boolean isItemBlock(){
        return false;
    }
}
