package versatile.internal.mc.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.item.IItem;
import versatile.internal.lang.LangDispatcher;
import versatile.internal.lang.LangWrapper;

import java.util.UUID;

public class ItemInternalWrapper extends Item implements IInternalItem {
    private UUID uuid = UUID.randomUUID();
    private IItem iItem;

    public ItemInternalWrapper(BlockInfo source, IItem item) {
        setRegistryName(source.getOwner(), source.getName());
        setUnlocalizedName(item.displayName().deduceName(LangWrapper.getInstance()));
        iItem = item;
    }

    @Override
    public IItem get() {
        return iItem;
    }

    @Override
    public UUID getUuid() {
        return uuid;
    }


    @Override
    public String getItemStackDisplayName(ItemStack stack) {
        return iItem.displayName().deduceName(LangDispatcher.instance());
    }
}