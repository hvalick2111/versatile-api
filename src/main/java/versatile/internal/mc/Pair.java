package versatile.internal.mc;

import java.util.Objects;

public class Pair<A, B>
{
    private A a;
    private B b;

    public Pair(A aIn, B bIn)
    {
        this.a = aIn;
        this.b = bIn;
    }

    /**
     * Get the first Object in the Tuple
     */
    public A getFirst()
    {
        return this.a;
    }

    /**
     * Get the second Object in the Tuple
     */
    public B getSecond()
    {
        return this.b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> hashTuple = (Pair<?, ?>) o;
        return Objects.equals(a, hashTuple.a) &&
                Objects.equals(b, hashTuple.b);
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b);
    }
}