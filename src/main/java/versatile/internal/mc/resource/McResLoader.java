package versatile.internal.mc.resource;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.IResource;
import versatile.internal.mc.ModResLocation;

import java.io.IOException;

public class McResLoader {
    public static IResource getResource(ModResLocation resourceLocation) {
        try {
            return Minecraft.getMinecraft().getResourceManager().getResource(resourceLocation);
        } catch (IOException ignored) {
            return null;
        }
    }
}
