package versatile.internal.mc.data;

import net.minecraft.nbt.NBTTagCompound;
import versatile.api.game.block.data.TagCompound;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class InternalTagCompound implements TagCompound {
    private NBTTagCompound compound = new NBTTagCompound();

    public InternalTagCompound(NBTTagCompound compound) {
        this.compound = compound.copy();
    }

    public InternalTagCompound(NBTTagCompound compound, boolean copy) {
        if (copy)
            this.compound = compound.copy();
        else
            this.compound = compound;
    }


    public NBTTagCompound copyCompound() {
        return compound.copy();
    }

    public NBTTagCompound getDirectCompound() {
        return compound;
    }

    public InternalTagCompound() {
    }

    private boolean has(String str) {
        return compound.hasKey(str);
    }

    @Override
    public String getStringOrDefault(String key, String defaultValue) {
        return has(key) ? compound.getString(key) : defaultValue;
    }


    @Override
    public String getStringOrEmpty(String key) {
        return has(key) ? compound.getString(key) : "";
    }


    @Override
    public int getIntOrDefault(String key, int defaultValue) {
        return has(key) ? compound.getInteger(key) : defaultValue;
    }

    @Override
    public boolean getBooleanOrDefault(String key, boolean defaultValue) {
        return has(key) ? compound.getBoolean(key) : defaultValue;
    }

    @Override
    public float getFloatOrDefault(String key, float defaultValue) {
        return has(key) ? compound.getFloat(key) : defaultValue;
    }

    @Override
    public double getDoubleOrDefault(String key, double defaultValue) {
        return has(key) ? compound.getDouble(key) : defaultValue;
    }

    @Override
    public long getLongOrDefault(String key, long defaultValue) {
        return has(key) ? compound.getLong(key) : defaultValue;
    }

    @Override
    public int[] getIntsOrDefault(String key, int[] defaultValue) {
        return has(key) ? compound.getIntArray(key) : defaultValue;
    }

    @Override
    public int[] getIntsOrEmpty(String key) {
        return has(key) ? compound.getIntArray(key) : new int[0];
    }

    @Override
    public byte[] getBytesOrDefault(String key, byte[] defaultValue) {
        return has(key) ? compound.getByteArray(key) : defaultValue;
    }

    @Override
    public byte[] getBytesOrEmpty(String key) {
        return has(key) ? compound.getByteArray(key) : new byte[0];
    }

    @Override
    public String getStringOrDefault(String key, Supplier<String> defaultValue) {
        return has(key) ? compound.getString(key) : defaultValue.get();
    }

    @Override
    public int getIntOrDefault(String key, Supplier<Integer> defaultValue) {
        return has(key) ? compound.getInteger(key) : defaultValue.get();
    }

    @Override
    public boolean getBooleanOrDefault(String key, Supplier<Boolean> defaultValue) {
        return has(key) ? compound.getBoolean(key) : defaultValue.get();
    }

    @Override
    public float getFloatOrDefault(String key, Supplier<Float> defaultValue) {
        return has(key) ? compound.getFloat(key) : defaultValue.get();
    }

    @Override
    public double getDoubleOrDefault(String key, Supplier<Double> defaultValue) {
        return has(key) ? compound.getDouble(key) : defaultValue.get();
    }

    @Override
    public long getLongOrDefault(String key, Supplier<Long> defaultValue) {
        return has(key) ? compound.getLong(key) : defaultValue.get();
    }

    @Override
    public int[] getIntsOrDefault(String key, Supplier<int[]> defaultValue) {
        return has(key) ? compound.getIntArray(key) : defaultValue.get();
    }

    @Override
    public byte[] getBytesOrDefault(String key, Supplier<byte[]> defaultValue) {
        return has(key) ? compound.getByteArray(key) : defaultValue.get();
    }

    @Override
    public TagCompound getTagOrEmpty(String key) {
        return has(key) ? new InternalTagCompound(compound.getCompoundTag(key)) : TagCompound.create();
    }

    @Override
    public TagCompound getTagOrDefault(String key, TagCompound defaultValue) {
        return has(key) ? new InternalTagCompound(compound.getCompoundTag(key)) : defaultValue;
    }

    @Override
    public TagCompound getTagOrFilled(String key, Consumer<TagCompound> defaultValue) {
        if (has(key))
            return new InternalTagCompound(compound.getCompoundTag(key));
        TagCompound compound = TagCompound.create();
        defaultValue.accept(compound);
        return compound;
    }

    @Override
    public boolean contains(String key) {
        return has(key);
    }

    @Override
    public void set(String key, String value) {
        compound.setString(key, value);
    }

    @Override
    public void set(String key, TagCompound value) {
        compound.setTag(key, ((InternalTagCompound) value).compound);
    }

    @Override
    public void set(String key, int value) {
        compound.setInteger(key, value);
    }

    @Override
    public void set(String key, boolean value) {
        compound.setBoolean(key, value);
    }

    @Override
    public void set(String key, float value) {
        compound.setFloat(key, value);
    }

    @Override
    public void set(String key, double value) {
        compound.setDouble(key, value);
    }

    @Override
    public void set(String key, long value) {
        compound.setLong(key, value);
    }

    @Override
    public void set(String key, int[] value) {
        compound.setIntArray(key, value);
    }

    @Override
    public void set(String key, byte[] value) {
        compound.setByteArray(key, value);
    }

    @Override
    public Set<String> keys() {
        return compound.getKeySet();
    }

    @Override
    public int size() {
        return compound.getSize();
    }

    @Override
    public boolean removeKey(String key) {
        boolean has = has(key);
        compound.removeTag(key);
        return has;
    }

    @Override
    public void extractFrom(TagCompound dest) {
        compound.merge(((InternalTagCompound) dest).compound);
    }

    @Override
    public <T> void setArray(String key, T[] array, Function<T, TagCompound> ser) {
        TagCompound compound = TagCompound.create();
        compound.set("size", array.length);
        for (int i = 0; i < array.length; i++)
            compound.set("element_" + i, ser.apply(array[i]));
        set(key, compound);
    }

    @Override
    public <T> T[] getArrayOrDefault(String key, Function<TagCompound, T> deSer, T[] defaultValue) {
        if (!contains(key))
            return defaultValue;
        Object[] objects = deserializeArray(key, deSer);
        @SuppressWarnings("unchecked")
        T[] ts = (T[]) Arrays.copyOf(objects, objects.length, defaultValue.getClass());
        return ts;
    }

    private <T> Object[] deserializeArray(String key, Function<TagCompound, T> deSer) {
        TagCompound compound = getTagOrEmpty(key);
        int size = compound.getIntOrDefault("size", 0);

        Object[] arr = new Object[size];

        for (int i = 0; i < size; i++)
            arr[i] = deSer.apply(compound.getTagOrEmpty("element_" + i));
        return arr;
    }

    @Override
    public TagCompound clone() {
        InternalTagCompound cloned = null;
        try {
            cloned = (InternalTagCompound) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        cloned.compound = compound.copy();
        return cloned;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InternalTagCompound that = (InternalTagCompound) o;
        return Objects.equals(compound, that.compound);
    }

    @Override
    public int hashCode() {
        return Objects.hash(compound);
    }

    @Override
    public String toString() {
        Set<String> keySet = compound.getKeySet();
        return keySet.toString();
    }
}
