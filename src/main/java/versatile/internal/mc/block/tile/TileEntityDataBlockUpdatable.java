package versatile.internal.mc.block.tile;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ITickable;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.world.Updatable;
import versatile.api.game.world.WorldController;
import versatile.internal.mc.block.BlockHandlers;
import versatile.internal.mc.world.WorldMapper;

public class TileEntityDataBlockUpdatable extends TileEntityDataBlock implements ITickable {

    private long nextTimeUpdated;

    public TileEntityDataBlockUpdatable() {
    }

    public TileEntityDataBlockUpdatable(BlockInfo blockInfo) {
        super(blockInfo);
    }

    @Override
    public void update() {
        Updatable capacitor = (Updatable) getCapacitor();
        long curTime = world.getTotalWorldTime();

        WorldController wController = WorldMapper.get(world);

        if (curTime >= nextTimeUpdated) {
            capacitor.update(wController, BlockHandlers.remapPos(pos));
            nextTimeUpdated = curTime + capacitor.updatePeriod();
        }
    }


    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        NBTTagCompound nbt = super.writeToNBT(compound);
        nbt.setLong("nextTimeUpdated", nextTimeUpdated);
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        nextTimeUpdated = compound.getLong("nextTimeUpdated");
    }
}