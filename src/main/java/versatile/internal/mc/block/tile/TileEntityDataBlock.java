package versatile.internal.mc.block.tile;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.block.data.DataCapacitor;
import versatile.internal.mc.block.BlockTileInternalWrapper;
import versatile.internal.mc.data.InternalTagCompound;

import java.util.function.Supplier;

public class TileEntityDataBlock extends TileEntity {
    private BlockInfo blockInfo;
    private DataCapacitor capacitor;

    public TileEntityDataBlock() {
    }

    public TileEntityDataBlock(BlockInfo blockInfo) {
        this.blockInfo = blockInfo;
        capacitor = getAllocator().get();
    }

    public Supplier<DataCapacitor> getAllocator(){
        return BlockTileInternalWrapper.getAllocatorsMap().get(blockInfo);
    }

    public DataCapacitor getCapacitor() {
        return capacitor;
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        InternalTagCompound tag = new InternalTagCompound();
        capacitor.saveDataTo(tag);
        NBTTagCompound blockData = tag.copyCompound();

        compound.setTag("block_versatile_data", blockData);
        compound.setString("block_info_owner", blockInfo.getOwner());
        compound.setString("block_info_name", blockInfo.getName());
        return super.writeToNBT(compound);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        String owner = compound.getString("block_info_owner");
        String name = compound.getString("block_info_name");
        blockInfo = new BlockInfo(owner, name);

        NBTTagCompound blockData = compound.getCompoundTag("block_versatile_data");
        InternalTagCompound tag = new InternalTagCompound(blockData);

        if(blockInfo.isEmpty()) return;
        capacitor = getAllocator().get();

        capacitor.restoreDataFrom(tag);
    }

    /*
    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? (T)inventory : super.getCapability(capability, facing);
    }

     */

}