package versatile.internal.mc.block;

import com.google.common.collect.HashBiMap;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.util.ResourceLocation;
import versatile.api.game.block.BlockInfo;

public class BlockCache {
    private static HashBiMap<Block, BlockInfo> blockInfoMap = HashBiMap.create(2000);


    static {
        init();
    }

    @SuppressWarnings("all")
    private static void init() {
        blockInfoMap.inverse();
        blockInfoMap.get(Blocks.DIRT);
    }


    public static Block getBlock(BlockInfo info) {
        return blockInfoMap.inverse().computeIfAbsent(info, in -> Block.REGISTRY.getObject(new ResourceLocation(info.getOwner(), info.getName())));
    }

    public static BlockInfo get(Block block) {
        return blockInfoMap.computeIfAbsent(block, BlockCache::getBlockInfo);
    }

    public static BlockInfo add(Block block, BlockInfo blockInfo) {
        blockInfoMap.put(block, blockInfo);
        return blockInfo;
    }

    private static BlockInfo getBlockInfo(Block block) {
        if (block == Blocks.AIR) return BlockInfo.EMPTY;
        ResourceLocation registryName = block.getRegistryName();
        return new BlockInfo(registryName.getResourceDomain(), registryName.getResourcePath());
    }
}
