package versatile.internal.mc.block;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.block.IBlock;
import versatile.api.game.block.data.DataCapacitor;
import versatile.internal.lang.LangWrapper;
import versatile.internal.mc.block.tile.TileEntityDataBlock;
import versatile.internal.mc.block.tile.TileEntityDataBlockUpdatable;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.UUID;
import java.util.function.Supplier;

import static versatile.internal.mc.block.BlockInternalWrapper.fromType;

public class BlockTileInternalWrapper extends Block implements IInternalBlock, ITileEntityProvider {
    private static HashMap<BlockInfo, Supplier<DataCapacitor>> allocatorsMap = new HashMap<>();
    private UUID uuid = UUID.randomUUID();
    private IBlock iBlock;
    private BlockInfo blockInfo;
    private boolean isUpdatable;

    public BlockTileInternalWrapper(
            BlockInfo source,
            IBlock block,
            boolean isUpdatable,
            Supplier<DataCapacitor> allocator
    ) {
        super(MaterialMapper.get(block.getMaterial()));
        setRegistryName(source.getOwner(), source.getName());
        setUnlocalizedName(block.getDisplayName().deduceName(LangWrapper.getInstance()));
        iBlock = block;
        this.isUpdatable = isUpdatable;
        BlockInfo info = new BlockInfo(source.getOwner(), source.getName());
        allocatorsMap.put(info, allocator);
        blockInfo = info;

        String type = fromType(block.getHarvestType());
        if(type != null)
            setHarvestLevel(type, block.getHarvestTier());
        setHardness(block.getHardness());
    }

    public static HashMap<BlockInfo, Supplier<DataCapacitor>> getAllocatorsMap() {
        return allocatorsMap;
    }

    @Override
    public IBlock get() {
        return iBlock;
    }

    @Override
    public UUID getUuid() {
        return uuid;
    }

    @Nullable
    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        if(isUpdatable)
            return new TileEntityDataBlockUpdatable(blockInfo);
        return new TileEntityDataBlock(blockInfo);
    }
}
