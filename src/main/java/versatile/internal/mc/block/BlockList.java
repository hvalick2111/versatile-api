package versatile.internal.mc.block;

import net.minecraft.block.Block;

import java.util.ArrayList;

public class BlockList <T extends Block & IInternalBlock>
        extends ArrayList<T> {

    @SuppressWarnings("unchecked")
    public void add(BlockInternalWrapper wrapper) {
        super.add((T) wrapper);
    }

    @SuppressWarnings("unchecked")
    public void add(BlockTileInternalWrapper wrapper) {
        super.add((T) wrapper);
    }
}
