package versatile.internal.mc.block;

import net.minecraft.block.material.Material;
import versatile.api.game.block.BlockMaterial;

import java.util.HashMap;

public class MaterialMapper {
    private static HashMap<BlockMaterial, Material> map = new HashMap<>();

    static {
        map.put(BlockMaterial.WOOD, Material.WOOD);
        map.put(BlockMaterial.GROUND, Material.GROUND);
        map.put(BlockMaterial.METAL, Material.IRON);
        map.put(BlockMaterial.ROCK, Material.ROCK);
        map.put(BlockMaterial.LIQUID, Material.WATER);
        map.put(BlockMaterial.AIR, Material.AIR);
    }


    public static Material get(BlockMaterial material){
        return map.get(material);
    }
}

