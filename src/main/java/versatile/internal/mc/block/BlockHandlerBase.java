package versatile.internal.mc.block;

import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import versatile.api.game.RequiredDependencies;
import versatile.api.game.block.BlockHandler;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.block.VanillaBlock;
import versatile.api.game.block.data.DataCapacitor;
import versatile.internal.FunctionMethod;
import versatile.internal.mc.block.tile.TileEntityDataBlock;

import javax.annotation.Nullable;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class BlockHandlerBase {
    private static Map<BlockInfo, List<BlockHandler>> eventHandlers = new HashMap<>();
    private static Set<Class<?>> eventDataHandlers = new HashSet<>();

    public void initHandlers(Map<BlockInfo, List<BlockHandler>> primary, Set<Class<? extends BlockHandler>> dataHandlers,  FunctionMethod<Class<? extends BlockHandler>, Method> getMethod) {
        Set<Class<? extends BlockHandler>> breakBlock = methodFiltration(dataHandlers, getMethod);
        eventDataHandlers.addAll(breakBlock);

        for (Map.Entry<BlockInfo, List<BlockHandler>> entry : primary.entrySet()) {
            List<BlockHandler> handlers = methodFiltration(entry.getValue(), handler ->
                    getMethod.apply(handler.getClass())
            );

            eventHandlers.put(entry.getKey(), handlers);
        }
    }


    public static VanillaBlock[] getBlockDependencies(Method method) {
        RequiredDependencies annotation = method.getAnnotation(RequiredDependencies.class);
        if (annotation == null) return new VanillaBlock[0];
        return annotation.value();
    }

    private static List<BlockHandler> methodFiltration(List<BlockHandler> input, FunctionMethod<BlockHandler, Method> filtered) {
        return input.stream().filter(handler -> {
            Method breakBlock;
            try {
                breakBlock = filtered.apply(handler);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return true;
            }
            return methodFilter(breakBlock);
        }).collect(Collectors.toList());
    }

    private static Set<Class<? extends BlockHandler>> methodFiltration(Collection<Class<? extends BlockHandler>> input, FunctionMethod<Class<? extends BlockHandler>, Method> filtered) {
        return input.stream().filter(handler -> {
            Method breakBlock;
            try {
                breakBlock = filtered.apply(handler);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return true;
            }
            return methodFilter(breakBlock);
        }).collect(Collectors.toSet());
    }

    private static boolean methodFilter(Method target) {
        try {
            VanillaBlock[] dependencies = getBlockDependencies(target);
            for (VanillaBlock block : dependencies) {
                if (!block.isPresent()) return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private static BlockHandler getDataHandler(World world, BlockPos pos, Set<Class<?>> activeHandlers){
        TileEntity tile = world.getTileEntity(pos);
        if(!(tile instanceof TileEntityDataBlock)) return null;
        TileEntityDataBlock dataBlock = (TileEntityDataBlock) tile;
        DataCapacitor capacitor = dataBlock.getCapacitor();
        if(capacitor instanceof BlockHandler)
            if(activeHandlers.contains(capacitor.getClass()))
                return (BlockHandler) capacitor;
        return null;
    }

    private static List<BlockHandler> addOrCreate(List<BlockHandler> current, BlockHandler handler){
        List<BlockHandler> handlers;
        if(current == null){
            handlers = new ArrayList<>();
        } else {
            handlers = new ArrayList<>(current);
        }
        handlers.add(handler);
        return handlers;
    }


    @Nullable
    public List<BlockHandler> getBlockHandlers(World world, BlockPos pos){
        IBlockState blockState = world.getBlockState(pos);
        BlockInfo info = BlockCache.get(blockState.getBlock());
        List<BlockHandler> blockHandlers = eventHandlers.get(info);

        BlockHandler dataHandler = getDataHandler(world, pos, eventDataHandlers);
        if(dataHandler != null)
            blockHandlers = addOrCreate(blockHandlers, dataHandler);


        return blockHandlers;
    }

}
