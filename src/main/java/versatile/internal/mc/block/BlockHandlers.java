package versatile.internal.mc.block;


import net.minecraft.util.EnumActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import versatile.api.game.block.BlockHandler;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.entity.Hand;
import versatile.api.game.entity.world.PlayerContainer;
import versatile.api.game.world.Pos;
import versatile.api.game.world.WorldController;
import versatile.internal.mc.world.WorldMapper;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static versatile.internal.mc.entity.InternalEntityController.get;
import static versatile.internal.mc.entity.world.InternalPlayerContainer.mapR;

public class BlockHandlers {

    private static final BlockHandlerBase breakBlock = new BlockHandlerBase();
    private static final BlockHandlerBase playerActivated = new BlockHandlerBase();
    private static final BlockHandlerBase blockPlaced = new BlockHandlerBase();


    //TODO: optimize handler filtration loop
    //TODO: asm method deleting
    public static void initHandlers(Map<BlockInfo, List<BlockHandler>> primary, Set<Class<? extends BlockHandler>> dataHandlers) {

        breakBlock.initHandlers(primary, dataHandlers, handler ->
                handler.getMethod("breakBlock", WorldController.class, Pos.class)
        );

        playerActivated.initHandlers(primary, dataHandlers, handler ->
                handler.getMethod("blockActivated", WorldController.class, Pos.class, PlayerContainer.class, Hand.class)
        );

        blockPlaced.initHandlers(primary, dataHandlers, handler ->
                handler.getMethod("placeBlock", WorldController.class, Pos.class, PlayerContainer.class)
        );
    }


    @SubscribeEvent(receiveCanceled = true, priority = EventPriority.HIGH)
    public void breakEvent(BlockEvent.BreakEvent event) {
        if (event.getWorld().isRemote) return;

        List<BlockHandler> blockHandlers = breakBlock.getBlockHandlers(event.getWorld(), event.getPos());
        if (blockHandlers == null) return;

        WorldController world = WorldMapper.get(event.getWorld());

        boolean used = false;
        for (BlockHandler handler : blockHandlers)
            if (!handler.breakBlock(world, remapPos(event.getPos())))
                used = true;

        if (used) event.setCanceled(true);
    }


    @SubscribeEvent(priority = EventPriority.HIGH)
    public void placeEvent(BlockEvent.PlaceEvent event) {
        if (event.getWorld().isRemote) return;

        List<BlockHandler> blockHandlers = breakBlock.getBlockHandlers(event.getWorld(), event.getPos());
        if (blockHandlers == null) return;

        WorldController world = WorldMapper.get(event.getWorld());

        boolean used = false;
        for (BlockHandler handler : blockHandlers)
            if (!handler.placeBlock(world, remapPos(event.getPos()),  (PlayerContainer) get(event.getPlayer())))
                used = true;

        if (used) event.setCanceled(true);
    }


    @SubscribeEvent(receiveCanceled = true, priority = EventPriority.HIGH)
    public void activated(PlayerInteractEvent.RightClickBlock event) {
        if (event.getEntityPlayer().isSneaking()) return;

        List<BlockHandler> blockHandlers = playerActivated.getBlockHandlers(event.getWorld(), event.getPos());

        if (blockHandlers == null) return;

        WorldController world = WorldMapper.get(event.getWorld());

        boolean used = false;
        for (BlockHandler handler : blockHandlers)
            if (handler.blockActivated(world, remapPos(event.getPos()), (PlayerContainer) get(event.getEntityPlayer()), mapR(event.getHand())))
                used = true;
        if (used) {
            event.setCanceled(true);
            event.setCancellationResult(EnumActionResult.SUCCESS);
        }
    }

    public static Pos remapPos(BlockPos pos) {
        return new Pos(pos.getX(), pos.getY(), pos.getZ());
    }
}
