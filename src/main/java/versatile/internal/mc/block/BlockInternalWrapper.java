package versatile.internal.mc.block;

import net.minecraft.block.Block;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.block.HarvestType;
import versatile.api.game.block.IBlock;
import versatile.internal.lang.LangWrapper;

import java.util.UUID;

public class BlockInternalWrapper extends Block implements IInternalBlock {
    private UUID uuid = UUID.randomUUID();
    private IBlock iBlock;

    public BlockInternalWrapper(
            BlockInfo source,
            IBlock block
    ) {
        super(MaterialMapper.get(block.getMaterial()));
        setRegistryName(source.getOwner(), source.getName());
        setUnlocalizedName(block.getDisplayName().deduceName(LangWrapper.getInstance()));
        String type = fromType(block.getHarvestType());
        if(type != null)
            setHarvestLevel(type, block.getHarvestTier());
        setHardness(block.getHardness());
        iBlock = block;
    }

    public static String fromType(HarvestType type){
        switch (type){
            case PICKAXE:
                return "pickaxe";
            case AXE:
                return "axe";
            case SHOVEL:
                return "shovel";
            default:
                return null;
        }
    }

    @Override
    public IBlock get() {
        return iBlock;
    }

    @Override
    public UUID getUuid() {
        return uuid;
    }

}
