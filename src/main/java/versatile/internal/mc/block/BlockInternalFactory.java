package versatile.internal.mc.block;

import versatile.api.game.block.*;
import versatile.api.game.lang.IDisplayName;
import versatile.api.resource.texture.TextureFactory;

public class BlockInternalFactory implements BlockFactory, IBlock {
    private final BlockInfo source;
    private IDisplayName displayName;
    private TextureFactory texture;
    private BlockMaterial material;
    private HarvestType type = HarvestType.NONE;
    private boolean createItemBlock = true;
    private int harvestTier = 0;
    private float hardness = 0.1f;

    public BlockInternalFactory(BlockInfo source) {
        this.source = source;
    }

    @Override
    public boolean isCreateItemBlock() {
        return createItemBlock;
    }

    @Override
    public HarvestType getHarvestType() {
        return type;
    }

    @Override
    public int getHarvestTier() {
        return harvestTier;
    }

    @Override
    public float getHardness() {
        return hardness;
    }

    @Override
    public void setCreateItemBlock(boolean createItemBlock) {
        this.createItemBlock = createItemBlock;
    }

    @Override
    public void setDisplayName(IDisplayName displayName) {
        this.displayName = displayName;
    }

    @Override
    public void setTexture(TextureFactory texture) {
        this.texture = texture;
    }

    @Override
    public void setMaterial(BlockMaterial material) {
        this.material = material;
    }

    @Override
    public void setHarvestType(HarvestType type) {
        this.type = type;
    }

    @Override
    public void setHardness(float hardness) {
        this.hardness = hardness;
    }

    @Override
    public void setHarvestTier(int tier) {
        this.harvestTier = tier;
    }

    @Override
    public BlockInfo getSource() {
        return source;
    }

    @Override
    public IDisplayName getDisplayName() {
        return displayName;
    }

    @Override
    public TextureFactory getTexture() {
        return texture;
    }

    @Override
    public BlockMaterial getMaterial() {
        return material == null ? BlockMaterial.METAL : material;
    }

}