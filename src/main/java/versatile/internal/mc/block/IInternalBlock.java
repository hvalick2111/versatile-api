package versatile.internal.mc.block;

import versatile.api.game.block.IBlock;
import versatile.internal.mc.registry.IUnique;

import java.util.function.Supplier;

public interface IInternalBlock extends Supplier<IBlock>, IUnique {
}
