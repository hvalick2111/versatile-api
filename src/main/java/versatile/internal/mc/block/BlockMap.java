package versatile.internal.mc.block;

import net.minecraft.block.Block;
import versatile.api.game.block.BlockInfo;

import java.util.HashMap;

public class BlockMap<T extends Block & IInternalBlock>
        extends HashMap<BlockInfo, T> {

    @SuppressWarnings("unchecked")
    public void put(BlockInfo info, BlockInternalWrapper wrapper) {
        super.put(info, (T) wrapper);
    }
}
