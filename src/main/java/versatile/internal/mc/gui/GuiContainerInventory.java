package versatile.internal.mc.gui;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import versatile.api.game.gui.GuiInventory;
import versatile.api.game.gui.GuiSlot;
import versatile.internal.VersatileMod;
import versatile.internal.mc.gui.container.ContainerInventory;

public class GuiContainerInventory extends GuiContainer {
    protected ResourceLocation texture = new ResourceLocation(VersatileMod.MODID, "mod/textures/gui/inventory/basic.png");
    private GuiInventory gui; //TODO: functional

    public GuiContainerInventory(ContainerInventory inventorySlotsIn) {
        super(inventorySlotsIn);
        this.gui = inventorySlotsIn.getGui();
    }


    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(texture);
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);

        ContainerInventory container = (ContainerInventory) inventorySlots;

        GuiSlot[] slots = container.getGui().slots();
        for (GuiSlot slot : slots) {
            int x1 = (width - getXSize()) / 2;
            int y1 = (height - getYSize()) / 2;

            if (slot.isVisible())
                this.drawTexturedModalRect(
                        x1 + (int) slot.getX() - 1,
                        y1 + (int) slot.getY() - 1,
                        238, 0, 18, 18
                );
        }
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {

    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);
    }
}
