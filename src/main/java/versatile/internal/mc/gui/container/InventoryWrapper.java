package versatile.internal.mc.gui.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import versatile.api.game.inventory.Inventory;
import versatile.api.game.item.ItemContainer;
import versatile.internal.mc.item.ItemMapper;

public class InventoryWrapper implements IInventory {
    private Inventory inventory;

    public InventoryWrapper(Inventory inventory) {
        this.inventory = inventory;
    }

    @Override
    public int getSizeInventory() {
        return inventory.size();
    }

    @Override
    public boolean isEmpty() {
        return inventory.isEmpty();
    }

    @Override
    public ItemStack getStackInSlot(int index) {
        return ItemMapper.unwrap(inventory.getItem(index));
    }

    @Override
    public ItemStack decrStackSize(int index, int count) {
        ItemStack itemstack;
        if (index >= 0 && index < getSizeInventory() && !(ItemMapper.unwrap(inventory.getItem(index))).isEmpty() && count > 0)
            itemstack = (ItemMapper.unwrap(inventory.getItem(index))).splitStack(count);
        else itemstack = ItemStack.EMPTY;

        if (!itemstack.isEmpty())
            this.markDirty();

        return itemstack;
    }

    @Override
    public ItemStack removeStackFromSlot(int index) {
        if (index >= 0 && index < getSizeInventory()) {
            ItemStack stackInSlot = getStackInSlot(index);
            setInventorySlotContents(index, ItemStack.EMPTY);
            return stackInSlot;
        }
        return ItemStack.EMPTY;
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        inventory.setItem(index, ItemMapper.wrap(stack));
    }

    @Override
    public int getInventoryStackLimit() {
        return 64;
    }

    @Override
    public void markDirty() {

    }

    @Override
    public boolean isUsableByPlayer(EntityPlayer player) {
        return true;
    }

    @Override
    public void openInventory(EntityPlayer player) {

    }

    @Override
    public void closeInventory(EntityPlayer player) {

    }

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        return true;
    }

    @Override
    public int getField(int id) {
        return 0;
    }

    @Override
    public void setField(int id, int value) {

    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public void clear() {
        for (int i = 0; i < inventory.size(); i++) {
            inventory.setItem(i, ItemContainer.EMPTY);
        }
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public boolean hasCustomName() {
        return false;
    }

    @Override
    public ITextComponent getDisplayName() {
        return (ITextComponent)(this.hasCustomName() ? new TextComponentString(this.getName()) : new TextComponentTranslation(this.getName(), new Object[0]));
    }
}
