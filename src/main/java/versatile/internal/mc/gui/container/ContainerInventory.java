package versatile.internal.mc.gui.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import versatile.api.game.gui.GuiInventory;
import versatile.api.game.gui.GuiSlot;
import versatile.internal.mc.item.ItemMapper;

public class ContainerInventory extends Container {
    private InventoryWrapper inventoryIn;
    private GuiInventory gui;

    public ContainerInventory(EntityPlayer player, GuiInventory gui) {
        this.gui = gui;
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 9; j++)
                this.addSlotToContainer(new Slot(player.inventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
        for (int i = 0; i < 9; i++)
            this.addSlotToContainer(new Slot(player.inventory, i, 8 + i * 18, 142));

        inventoryIn = new InventoryWrapper(gui.getInventory());
        genInv(gui);

//        if(inventory.size() <= 24)
//            genSlots(inventory, 8, 2);
//        else
//            genSlots(inventory, 9, 0);
    }

    public GuiInventory getGui() {
        return gui;
    }

    private Slot createSlot(IInventory inventoryIn, int index, int xPosition, int yPosition, GuiSlot slot) {
        if (!gui.canInteract())
            return new SlotNoInteract(inventoryIn, index, xPosition, yPosition);
        return new Slot(inventoryIn, index, xPosition, yPosition) {
            @Override
            public boolean isItemValid(ItemStack stack) {
                return super.isItemValid(stack) && slot.canPutItem(ItemMapper.wrap(stack));
            }
        };
    }


    private void genInv(GuiInventory inventory) {
        GuiSlot[] slots = inventory.slots();
        for (int i = 0; i < slots.length; i++) {
            GuiSlot slot = slots[i];
            int x = (int) slot.getX();
            int y = (int) slot.getY();

            if (slot.isVisible())
                addSlotToContainer(createSlot(inventoryIn, i, x, y, slot));
        }
    }

    public InventoryWrapper getInventoryIn() {
        return inventoryIn;
    }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return true;
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
        final int playerInvSize = 36;
        ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);

        if (slot != null && slot.getHasStack()) {
            ItemStack stack = slot.getStack();
            itemstack = stack.copy();

            if (index >= playerInvSize) {
                if (!this.mergeItemStack(stack, 0, playerInvSize, true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.mergeItemStack(stack, playerInvSize, playerInvSize + gui.countVisibleSlots(), false)) {
                return ItemStack.EMPTY;
            }

            if (stack.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
        }

        return itemstack;
    }
}
