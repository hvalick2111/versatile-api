package versatile.internal.mc.client;

import net.minecraft.client.resources.IResourceManager;
import net.minecraftforge.client.resource.IResourceType;
import net.minecraftforge.client.resource.ISelectiveResourceReloadListener;
import versatile.internal.VersatileMod;
import versatile.internal.lang.LangDispatcher;

import java.util.function.Predicate;

public class ResourceReloadListener implements ISelectiveResourceReloadListener {
    @Override
    public void onResourceManagerReload(IResourceManager resourceManager, Predicate<IResourceType> resourcePredicate) {
        LangDispatcher.instance().load(VersatileMod.getMods().keySet());
    }
}
