package versatile.internal.mc.client.texture;

import versatile.api.resource.texture.RenderTexture;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

public class URLRenderTexture implements RenderTexture {
    private URL url;
    private String name;

    URLRenderTexture(URL url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public String uniqueName() {
        return name;
    }

    @Override
    public boolean makeSuitable() {
        return true;
    }

    @Override
    public BufferedImage image() {
        try {
            return ImageIO.read(url);
        } catch (IOException e) {
            e.printStackTrace();
            return ResourceRenderTexture.noImage();
        }
    }
}

