package versatile.internal.mc.client.texture;

import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class DefTextureRegister {
    private static Map<String, DefTextureAtlasSprite> textureAtlasSprites = new HashMap<>();

    @SubscribeEvent
    public void event(TextureStitchEvent.Pre event) {
        for (TextureAtlasSprite sprite : textureAtlasSprites.values())
            event.getMap().setTextureEntry(sprite);
    }

    public static DefTextureAtlasSprite computeIfAbsent(String name, Function<String, DefTextureAtlasSprite> function){
        return textureAtlasSprites.computeIfAbsent(name, function);
    }
}
