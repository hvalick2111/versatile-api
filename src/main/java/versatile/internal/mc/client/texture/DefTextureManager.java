package versatile.internal.mc.client.texture;

import versatile.api.resource.texture.RenderTexture;
import versatile.api.resource.texture.TextureManager;

import java.awt.*;
import java.net.URL;

public class DefTextureManager implements TextureManager {
    private String modid;
    private boolean item;

    public DefTextureManager(String modid, boolean item) {
        this.modid = modid;
        this.item = item;
    }

    public DefTextureManager(String modid) {
        this.modid = modid;
    }

    @Override
    public RenderTexture loadTextureFromResource(String path) {
        if(item) return new ResourceRenderTexture(modid, "textures/items/" + path + ".png");
        return new ResourceRenderTexture(modid, "textures/blocks/" + path + ".png");
    }

    @Override
    public RenderTexture loadTextureFromURL(URL path) {
        return new URLRenderTexture(path, path.toString());
    }


    @Override
    public RenderTexture loadTextureFromColor(Color color) {
        return new SimpleRenderTexture(color);
    }
}