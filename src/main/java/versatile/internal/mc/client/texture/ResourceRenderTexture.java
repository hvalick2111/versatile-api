package versatile.internal.mc.client.texture;

import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.client.resources.IResource;
import versatile.api.resource.texture.RenderTexture;
import versatile.internal.VersatileMod;
import versatile.internal.mc.ModResLocation;
import versatile.internal.mc.resource.McResLoader;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Arrays;

public class ResourceRenderTexture implements RenderTexture {
    private ModResLocation location;
    private boolean tryLoadNoImage;

    ResourceRenderTexture(String modid, String location) {
        this.location = new ModResLocation(modid, location);
        tryLoadNoImage = true;
    }

    private ResourceRenderTexture(String modid, String location, boolean tryLoadNoImage) {
        this.location = new ModResLocation(modid, location);
        this.tryLoadNoImage = tryLoadNoImage;
    }

    @Override
    public String uniqueName() {
        return location.toString();
    }

    @Override
    public boolean makeSuitable() {
        return false;
    }

    @Override
    public BufferedImage image() {
        try (IResource resource = McResLoader.getResource(location)) {
            return TextureUtil.readBufferedImage(resource.getInputStream());
        } catch (Exception e) {
            System.err.println("Couldn't load texture with location " + location.toString());
            e.printStackTrace();
            if(tryLoadNoImage)
                return noImage();
        }
        BufferedImage image = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
        int[] data = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
        Arrays.fill(data, Color.BLACK.getRGB());
        return image;
    }

    private static BufferedImage cached;

    static BufferedImage noImage(){
        if(cached != null) return cached;
        cached = new ResourceRenderTexture(VersatileMod.MODID, "textures/no_image.png", false).image();
        return cached;
    }
}

