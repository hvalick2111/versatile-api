package versatile.internal.mc.client.texture;

import com.google.common.collect.Lists;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import versatile.api.resource.texture.RenderTexture;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.function.Function;

public class DefTextureAtlasSprite extends TextureAtlasSprite {
    private RenderTexture texture;

    public DefTextureAtlasSprite(RenderTexture texture) {
        super(texture.uniqueName());
        this.texture = texture;
    }

    private static int[][] getFrameTextureData(int[][] data, int rows, int columns, int p_147962_3_) {
        int[][] emptyData = new int[data.length][];
        for (int i = 0; i < data.length; ++i) {
            int[] curData = data[i];

            if (curData != null) {
                emptyData[i] = new int[(rows >> i) * (columns >> i)];
                System.arraycopy(curData, p_147962_3_ * emptyData[i].length, emptyData[i], 0, emptyData[i].length);
            }
        }
        return emptyData;
    }

    @Override
    public boolean hasCustomLoader(IResourceManager manager, ResourceLocation location) {
        return true;
    }

    @Override
    public boolean load(IResourceManager manager, ResourceLocation location, Function<ResourceLocation, TextureAtlasSprite> textureGetter) {
        try {

            BufferedImage bufferedimage = texture.image();

            this.width = bufferedimage.getWidth();
            this.height = bufferedimage.getHeight();

            if (width != height)
                throw new RuntimeException("broken aspect ratio and not a suitable");

            if (texture.makeSuitable()) {
                bufferedimage = resizeImage(bufferedimage);
            }

            this.animationMetadata = null;
            this.setFramesTextureData(Lists.newArrayList());
            this.frameCounter = 0;
            this.tickCounter = 0;
            this.width = bufferedimage.getWidth();
            this.height = bufferedimage.getHeight();

            int[][] imageData = new int[Minecraft.getMinecraft().gameSettings.mipmapLevels + 1][];
            imageData[0] = new int[bufferedimage.getWidth() * bufferedimage.getHeight()];
            bufferedimage.getRGB(0, 0, bufferedimage.getWidth(), bufferedimage.getHeight(), imageData[0], 0, bufferedimage.getWidth());

            this.framesTextureData.add(imageData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private static BufferedImage resizeImage(BufferedImage image) {
        int height = image.getHeight();
        int width = image.getWidth();

        int min = Math.min(width, height);

        int i = 1;
        while (i <= min) {
            i *= 2;
        }
        return scale(image, i, i);
    }

    private static BufferedImage scale(BufferedImage before, int w2, int h2) {
        int w = before.getWidth();
        int h = before.getHeight();
        BufferedImage after = new BufferedImage(w2, h2, BufferedImage.TYPE_INT_ARGB);
        AffineTransform scaleInstance = AffineTransform.getScaleInstance(w2/(double)w, h2/(double)h);
        AffineTransformOp scaleOp
                = new AffineTransformOp(scaleInstance, AffineTransformOp.TYPE_BILINEAR);

        scaleOp.filter(before, after);
        return after;
    }

}
