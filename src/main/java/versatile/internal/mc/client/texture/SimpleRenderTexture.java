package versatile.internal.mc.client.texture;

import versatile.api.resource.texture.RenderTexture;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Arrays;

public class SimpleRenderTexture implements RenderTexture {
    private final Color color;

    SimpleRenderTexture(Color color) {
        this.color = color;
    }

    @Override
    public String uniqueName() {
        return color.toString();
    }

    @Override
    public boolean makeSuitable() {
        return true;
    }

    @Override
    public BufferedImage image() {
        try {
            BufferedImage image = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
            int[] data = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
            Arrays.fill(data, color.getRGB());
            return image;
        } catch (Exception e) {
            e.printStackTrace();
            return ResourceRenderTexture.noImage();
        }
    }
}

