package versatile.internal.mc.client.model;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ICustomModelLoader;
import net.minecraftforge.client.model.IModel;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.client.FMLClientHandler;
import versatile.internal.VersatileMod;
import versatile.internal.mc.block.IInternalBlock;
import versatile.internal.mc.item.IInternalItem;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class VersatileModelLoader <I extends Item & IInternalItem, B extends Block & IInternalBlock> implements ICustomModelLoader {
    private final Map<String, B> blocks;
    private final Map<String, I> items;

    public VersatileModelLoader(
            Map<String, List<B>> blocks,
            Map<String, List<I>> items
    ) {
        this.blocks = uniqueMap(blocks);
        this.items = uniqueMap2(items);
    }

    private static <B extends Block & IInternalBlock> Map<String, B> uniqueMap(Map<String, List<B>> registry){
        return registry.values().stream().flatMap(Collection::stream).collect(Collectors.toMap(u -> u.getUuid().toString(), u -> u));
    }

    private static <I extends Item & IInternalItem> Map<String, I> uniqueMap2(Map<String, List<I>> registry){
        return registry.values().stream().flatMap(Collection::stream).collect(Collectors.toMap(u -> u.getUuid().toString(), u -> u));
    }

    @Override
    public void onResourceManagerReload(IResourceManager resourceManager) {
        VersatileMod.getBlocks().forEach((key, blocks) -> {
            blocks.forEach(block -> ModelLoader.setCustomStateMapper(block, blockModel -> {
                Map<IBlockState, ModelResourceLocation> locationMap = new HashMap<>();
                locationMap.put(blockModel.getDefaultState(), new ModelResourceLocation(key + ":" + block.getUuid().toString()));
                return locationMap;
            }));
        });

        VersatileMod.getItems().forEach((key, items) -> {
            items.forEach(item -> {
                ModelLoader.setCustomModelResourceLocation(item,0,
                    new ModelResourceLocation(key + ":" + item.getUuid().toString())
                );
            });
        });
    }

    @Override
    public boolean accepts(ResourceLocation modelLocation) {
        if(VersatileMod.isModLoaded(modelLocation.getResourceDomain())){
            String uuid = modelLocation.getResourcePath();
            B wrapperBlock = blocks.get(uuid);
            I wrapperItem = items.get(uuid);
            if(wrapperBlock == null && wrapperItem == null) //TODO отедельные имена для блоков и предметов
                throw new RuntimeException("Invalid model id: " + uuid);
            return true;
        }
        return false;
    }

    @Override
    public IModel loadModel(ResourceLocation modelLocation) {
        String uuid = modelLocation.getResourcePath();
        I wrapperItem = items.get(uuid);
        B wrapperBlock = blocks.get(uuid);
        if(wrapperItem == null) {
            if(wrapperBlock == null)
                throw new RuntimeException("Invalid model id: " + uuid);
            if (FMLClientHandler.instance().hasOptifine())
                return new BlockStandardModelOptifine<>(wrapperBlock, modelLocation.getResourceDomain());
            else
                return new BlockStandardModel<>(wrapperBlock, modelLocation.getResourceDomain());
        }
        if(wrapperItem.isItemBlock())
            return new ItemBlockStandardModel<>(wrapperItem, modelLocation.getResourceDomain());
        return new ItemStandardModel<>(wrapperItem, modelLocation.getResourceDomain());
    }
}
