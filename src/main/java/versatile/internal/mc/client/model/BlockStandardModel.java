package versatile.internal.mc.client.model;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModel;
import net.minecraftforge.common.model.IModelState;
import versatile.api.resource.texture.RenderTexture;
import versatile.api.resource.texture.TextureFactory;
import versatile.internal.mc.block.IInternalBlock;
import versatile.internal.mc.client.modelcreator.ModelBaker;
import versatile.internal.mc.client.texture.DefTextureAtlasSprite;
import versatile.internal.mc.client.texture.DefTextureManager;
import versatile.internal.mc.client.texture.DefTextureRegister;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

public class BlockStandardModel<B extends Block & IInternalBlock> implements IModel {
    private final DefTextureAtlasSprite atlas;

    BlockStandardModel(B wrapper, String modid) {
        TextureFactory textureFactory = wrapper.get().getTexture();
        RenderTexture texture;
        try {
            texture = textureFactory.create(new DefTextureManager(modid));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to initialize texture");
        }
        this.atlas = DefTextureRegister.computeIfAbsent(texture.uniqueName(), k -> new DefTextureAtlasSprite(texture));
    }

    @Override
    public IBakedModel bake(IModelState state, VertexFormat format, Function<ResourceLocation, TextureAtlasSprite> bakedTextureGetter) {
        HashMap<EnumFacing, List<BakedQuad>> sideQuads = new HashMap<>();

        for (EnumFacing facing : EnumFacing.values()) {
            ModelBaker baker = ModelBaker.INSTANCE;
            baker.begin(state, format);
            baker.setTexture(atlas);
            baker.putQuad(0, 0, 0, 0.5f, 0.5f, 0.5f, atlas.getMaxU(), atlas.getMinU(), atlas.getMinV(), atlas.getMaxV(), facing);
            sideQuads.put(facing, baker.bake());
        }
        return new BlockStandardBakedModel(sideQuads, new ArrayList<>(), atlas);
    }
}
