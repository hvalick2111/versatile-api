package versatile.internal.mc.client.model;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModel;
import net.minecraftforge.common.model.IModelState;
import versatile.api.resource.texture.RenderTexture;
import versatile.api.resource.texture.TextureFactory;
import versatile.internal.mc.block.IInternalBlock;
import versatile.internal.mc.client.texture.DefTextureAtlasSprite;
import versatile.internal.mc.client.texture.DefTextureManager;
import versatile.internal.mc.client.texture.DefTextureRegister;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

public class BlockStandardModelOptifine<B extends Block & IInternalBlock> implements IModel {
    private final DefTextureAtlasSprite atlas;
    private static MethodHandle makeBakedQuad = null;

    static {
        try {
            MethodHandles.Lookup publicLookup = MethodHandles.publicLookup();
            Class<?> clazz = Class.forName("net.optifine.model.BlockModelUtils");
            MethodType type = MethodType.methodType(BakedQuad.class, EnumFacing.class, TextureAtlasSprite.class, int.class);
            makeBakedQuad = publicLookup.findStatic(clazz, "makeBakedQuad", type);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    BlockStandardModelOptifine(B wrapper, String modid) {
        TextureFactory textureFactory = wrapper.get().getTexture();
        RenderTexture texture;
        try {
            texture = textureFactory.create(new DefTextureManager(modid));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to initialize texture");
        }
        this.atlas = DefTextureRegister.computeIfAbsent(texture.uniqueName(), k -> new DefTextureAtlasSprite(texture));
    }

    @Override
    public IBakedModel bake(IModelState state, VertexFormat format, Function<ResourceLocation, TextureAtlasSprite> bakedTextureGetter) {
        HashMap<EnumFacing, List<BakedQuad>> sideQuads = new HashMap<>();

        for (EnumFacing facing : EnumFacing.values()) {
            BakedQuad invoke = null;
            try {
                invoke = (BakedQuad) makeBakedQuad.invoke(facing, atlas, 0);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            sideQuads.put(facing, Collections.singletonList(invoke));
        }
        return new BlockStandardBakedModel(sideQuads, new ArrayList<>(), atlas);
    }
}
