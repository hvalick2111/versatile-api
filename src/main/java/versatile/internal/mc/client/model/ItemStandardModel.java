package versatile.internal.mc.client.model;

import com.google.common.collect.ImmutableList;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModel;
import net.minecraftforge.client.model.ItemLayerModel;
import net.minecraftforge.common.model.IModelState;
import versatile.api.resource.texture.RenderTexture;
import versatile.api.resource.texture.TextureFactory;
import versatile.internal.mc.client.texture.DefTextureAtlasSprite;
import versatile.internal.mc.client.texture.DefTextureManager;
import versatile.internal.mc.client.texture.DefTextureRegister;
import versatile.internal.mc.item.IInternalItem;

import java.util.Optional;
import java.util.function.Function;

public class ItemStandardModel<I extends Item & IInternalItem> implements IModel {
    private final DefTextureAtlasSprite atlas;

    ItemStandardModel(I wrapper, String modid) {
        TextureFactory textureFactory = wrapper.get().texture();
        RenderTexture texture;
        try {
            texture = textureFactory.create(new DefTextureManager(modid, true));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to initialize texture");
        }
        this.atlas = DefTextureRegister.computeIfAbsent(texture.uniqueName(), k -> new DefTextureAtlasSprite(texture));
    }

    @Override
    public IBakedModel bake(IModelState state, VertexFormat format, Function<ResourceLocation, TextureAtlasSprite> bakedTextureGetter) {
        ImmutableList<BakedQuad> quads = ItemLayerModel.getQuadsForSprite(0, atlas, format, state.apply(Optional.empty()));
        return new ItemStandardBakedModel(quads, atlas);
    }

    //    public IBakedModel bakeImpl(IModelState state, final VertexFormat format, Function<ResourceLocation, TextureAtlasSprite> bakedTextureGetter)
//    {
//        if(!Attributes.moreSpecific(format, Attributes.DEFAULT_BAKED_FORMAT))
//            throw new IllegalArgumentException("can't bake vanilla models to the format that doesn't fit into the default one: " + format);
//        ModelBlock model = this.model;
//
//        List<TRSRTransformation> newTransforms = Lists.newArrayList();
//        for(int i = 0; i < model.getElements().size(); i++)
//        {
//            BlockPart part = model.getElements().get(i);
//            newTransforms.add(animation.getPartTransform(state, part, i));
//        }
//
//        ItemCameraTransforms transforms = model.getAllTransforms();
//        Map<ItemCameraTransforms.TransformType, TRSRTransformation> tMap = Maps.newEnumMap(ItemCameraTransforms.TransformType.class);
//        tMap.putAll(PerspectiveMapWrapper.getTransforms(transforms));
//        tMap.putAll(PerspectiveMapWrapper.getTransforms(state));
//        IModelState perState = new SimpleModelState(ImmutableMap.copyOf(tMap));
//
//        if(hasItemModel(model))
//        {
//            return new ItemLayerModel(model).bake(perState, format, bakedTextureGetter);
//        }
//        if(isCustomRenderer(model)) return new BuiltInModel(transforms, model.createOverrides());
//        return bakeNormal(model, perState, state, newTransforms, format, bakedTextureGetter, uvlock);
//    }
}
