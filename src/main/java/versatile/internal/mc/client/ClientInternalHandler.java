package versatile.internal.mc.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.IReloadableResourceManager;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoaderRegistry;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import versatile.internal.VersatileMod;
import versatile.internal.mc.client.model.VersatileModelLoader;
import versatile.internal.mc.client.texture.DefTextureRegister;

public class ClientInternalHandler {
    @SuppressWarnings("unchecked")
    @SubscribeEvent
    public void eventRegisterModel(ModelRegistryEvent event) {
        VersatileModelLoader loader = new VersatileModelLoader(VersatileMod.getBlocks(), VersatileMod.getItems());
        ModelLoaderRegistry.registerLoader(loader);
    }

    public static void load(){
        MinecraftForge.EVENT_BUS.register(new DefTextureRegister());
        IReloadableResourceManager manager = (IReloadableResourceManager) Minecraft.getMinecraft().getResourceManager();
        manager.registerReloadListener(new ResourceReloadListener());

    }
}
