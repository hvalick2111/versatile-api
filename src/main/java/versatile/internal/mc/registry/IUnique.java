package versatile.internal.mc.registry;

import java.util.UUID;

public interface IUnique {
    UUID getUuid();
}
