package versatile.internal.mc.packet;

import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import versatile.internal.VersatileMod;
import versatile.internal.mc.packet.messages.PacketOpenInventory;

public class PacketRegister {
    public static final SimpleNetworkWrapper NETWORK = NetworkRegistry.INSTANCE.newSimpleChannel(VersatileMod.MODID);
    private static short id;

    public static void registerAll() {
        NETWORK.registerMessage(PacketOpenInventory.class, PacketOpenInventory.class, id++, Side.CLIENT);
    }
}
