package versatile.internal.mc.packet.messages;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import versatile.api.game.GameSide;
import versatile.api.game.gui.GuiInventory;
import versatile.api.game.inventory.ArrayInventory;
import versatile.api.game.item.ItemContainer;
import versatile.internal.mc.data.InternalTagCompound;
import versatile.internal.mc.gui.GuiContainerInventory;
import versatile.internal.mc.gui.container.ContainerInventory;

public class PacketOpenInventory extends PacketDefaultBase<PacketOpenInventory> {
    @SideOnly(Side.CLIENT)
    private static GuiInventory lastClientGui;

    @SideOnly(Side.CLIENT)
    public static void setLastClientGui(GuiInventory lastClientGui) {
        PacketOpenInventory.lastClientGui = lastClientGui;
    }

    private int windowId;
    private int count;
    private GuiInventory gui;
    private GameSide effective;

    public PacketOpenInventory() {
    }

    public PacketOpenInventory(int windowId, int count, GuiInventory gui, GameSide effective) {
        this.windowId = windowId;
        this.count = count;
        this.gui = gui;
        this.effective = effective;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        windowId = buf.readInt();
        count = buf.readInt();
        effective = buf.readBoolean() ? GameSide.CLIENT : GameSide.SERVER;


        if (effective == GameSide.SERVER) {
            NBTTagCompound guiTag = ByteBufUtils.readTag(buf);
            ArrayInventory inventory = new ArrayInventory() {
                ItemContainer[] containers = new ItemContainer[count];

                @Override
                public ItemContainer[] getItems() {
                    return containers;
                }
            };

            gui = new GuiInventory(inventory);
            gui.restoreDataFrom(new InternalTagCompound(guiTag));
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(windowId);
        buf.writeInt(count);
        buf.writeBoolean(effective == GameSide.CLIENT);

        if (effective == GameSide.SERVER) {
            NBTTagCompound ser = ((InternalTagCompound) gui.serialize()).getDirectCompound();
            ByteBufUtils.writeTag(buf, ser);
        }
    }

    @Override
    public void client(EntityPlayer player) {

        Minecraft.getMinecraft().addScheduledTask(() -> {

            FMLCommonHandler.instance().showGuiScreen(new GuiContainerInventory(new ContainerInventory(player, effective == GameSide.CLIENT ? lastClientGui : gui)));
            lastClientGui = null;
            player.openContainer.windowId = windowId;
        });
    }
}
