package versatile.internal.mc.packet.messages;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public abstract class PacketDefaultBase <T extends PacketDefaultBase<?>> implements IMessage, IMessageHandler<T, IMessage>
{
    public PacketDefaultBase()
    {
    }

    public void server(EntityPlayer player){

    }

    public void client(EntityPlayer player){

    }

    @Override
    public IMessage onMessage(PacketDefaultBase message, MessageContext ctx) {
        if (ctx.side.isServer())
            message.server(ctx.getServerHandler().player);
        else
            message.client(Minecraft.getMinecraft().player);
        return null;
    }
}
