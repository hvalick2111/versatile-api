package versatile.internal.mc;

import net.minecraft.util.ResourceLocation;

public class ModResLocation extends ResourceLocation {
    public ModResLocation(String resourceDomainIn, String resourcePathIn) {
        super(resourceDomainIn, "mod/" + resourcePathIn.toLowerCase());
    }

}
