package versatile.internal;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ArrayUtil {
    @SuppressWarnings("unchecked")
    public static <T> T[] excludeNulls(T[] array) {
        if(array.length == 0) return null;
        List<T> list = new ArrayList<>();
        for (T t : array)
            if (t != null)
                list.add(t);
        return list.toArray((T[]) Array.newInstance(array.getClass(), 0));
    }
}
