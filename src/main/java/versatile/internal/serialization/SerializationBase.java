package versatile.internal.serialization;

import versatile.api.game.block.data.TagCompound;
import versatile.api.game.block.data.TagSerialization;

import java.io.*;

public class SerializationBase implements TagSerialization {

    @Override
    public <T extends Serializable> TagCompound serialize(T object) {
        TagCompound compound = TagCompound.create();

        if(object != null) {
            byte[] array;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try (ObjectOutputStream stream = new ObjectOutputStream(out)) {
                stream.writeObject(object);
                array = out.toByteArray();
            } catch (IOException e) {
                throw new IllegalArgumentException("This object cannot be serialized", e);
            }
            compound.set("data", array);
        } else {
            compound.set("object_null", true);
        }

        return compound;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends Serializable> T deserialize(TagCompound data, Class<T> ignored) {
        if(data.getBooleanOrDefault("object_null", false)) return null;
        byte[] bytes = data.getBytesOrDefault("data", () -> {
            throw new IllegalArgumentException("Invalid input tag structure");
        });

        try (ObjectInputStream stream = new ObjectInputStream(new ByteArrayInputStream(bytes))){
            return (T) stream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new IllegalArgumentException("Invalid input tag bytes structure", e);
        }
    }
}
