package versatile.internal;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.eventbus.EventBus;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.*;
import net.minecraftforge.fml.common.discovery.ASMDataTable;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.FMLThrowingEventBus;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import versatile.api.IMod;
import versatile.api.Modification;
import versatile.api.game.block.*;
import versatile.api.game.block.data.DataCapacitor;
import versatile.api.game.entity.EntityHandler;
import versatile.api.game.entity.EntityHandlerInitializer;
import versatile.api.game.item.ItemFactory;
import versatile.api.game.item.ItemInitializer;
import versatile.api.game.item.VanillaItem.Property;
import versatile.api.game.world.Updatable;
import versatile.api.internal.*;
import versatile.api.mod.InfoEnvironment;
import versatile.internal.mc.Pair;
import versatile.internal.mc.block.*;
import versatile.internal.mc.block.tile.TileEntityDataBlock;
import versatile.internal.mc.block.tile.TileEntityDataBlockUpdatable;
import versatile.internal.mc.client.ClientInternalHandler;
import versatile.internal.mc.data.InternalTagCompound;
import versatile.internal.mc.entity.EntityCache;
import versatile.internal.mc.entity.EntityHandlers;
import versatile.internal.mc.entity.InternalEntity;
import versatile.internal.mc.item.*;
import versatile.internal.mc.packet.PacketRegister;
import versatile.internal.mod.InfoDispatcher;
import versatile.internal.mod.VModContainer;
import versatile.internal.serialization.SerializationBase;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;


@Mod.EventBusSubscriber
@Mod(modid = VersatileMod.MODID)
public class VersatileMod {
    public static final String MODID = "versatile";

    @Mod.Instance(MODID)
    public static VersatileMod INSTANCE;

    private static Map<String, ModStorage> mods = new HashMap<>();
    private static Map<IMod, ModStorage> aliasesMods = new HashMap<>();
    private static boolean isModsInit;
    private static Map<String, BlockList<?>> blocks = new HashMap<>();
    private static Map<String, ItemList<?>> items = new HashMap<>();

    private static Map<BlockInfo, Block> blockInfoMap = new HashMap<>(200);
    private static Map<BlockInfo, Item> itemInfoMap = new HashMap<>(200);
    private static Map<BlockInfo, List<BlockHandler>> blockHandlerMap = new HashMap<>();
    private static Set<Class<? extends BlockHandler>> blockDataHandlerMap = new HashSet<>();
    private static Map<BlockInfo, List<EntityHandler<?>>> entityHandlerMap = new HashMap<>();
    private static Map<BlockInfo, Supplier<DataCapacitor>> blockCapacitorMap = new HashMap<>();

    public static Map<String, ModStorage> getMods() {
        return mods;
    }

    public static boolean isModLoaded(String modid) {
        return mods.containsKey(modid);
    }

    public static Map<String, BlockList<?>> getBlocks() {
        return blocks;
    }

    public static Map<String, ItemList<?>> getItems() {
        return items;
    }

    private static BlockInfo of(Block block) {
        return BlockCache.get(block);
    }

    public static boolean isVersatileBlock(BlockInfo info) {
        return blockInfoMap.containsKey(info);
    }

    public static BlockInfo ofAdd(Block block) {
        BlockInfo info = BlockCache.get(block);
        blockInfoMap.put(info, block);
        return info;
    }

    public static BlockInfo ofAdd(Item item) {
        BlockInfo info = ItemCache.get(item);
        itemInfoMap.put(info, item);
        return info;
    }

    public static Map<BlockInfo, List<EntityHandler<?>>> getEntityHandlerMap() {
        return entityHandlerMap;
    }

    public static Map<BlockInfo, List<BlockHandler>> getBlockHandlerMap() {
        return blockHandlerMap;
    }

    private static Property[] of(Property... properties){
        return properties;
    }

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        PacketRegister.registerAll();
        ItemPropertyMapper.init();

        INSerializationFactory.initSerializationBase(new SerializationBase());
        INGuiUtil.setDefaultSlotSize(18f);

        INBlockRegistry.setLoggerUndefined(s -> {
            System.out.println("Block \"" + s + "\" is Empty!");
        });

        INItemRegistry.setLoggerUndefined(s -> {
            System.out.println("Item \"" + s + "\" is Empty!");
        });

        HashMap<String, BlockInfo> blocks = VanillaBlockGenerator.generate();

        INBlockRegistry.initVanillaBlocks(blocks);


        INItemUtil.initAliasesCallback(ItemPropertyMapper::getAliases);

        HashMap<BlockInfo, Property[]> itemProperties = new HashMap<>();

        for (Map.Entry<ItemPattern, Pair<ItemPattern, Property>> entry : ItemPropertyMapper.getPropertyMap().entrySet()){
            if(entry.getKey().getMeta() == 0){
                itemProperties.put(ItemCache.get(entry.getKey().getItem()), of(entry.getValue().getSecond()));
            }
        }

        INItemUtil.initPropertiesMap(itemProperties);


        INItemRegistry.initVanillaItems(VanillaItemGenerator.generate());

        ItemPropertyMapper.initAliases();


        HashMap<String, BlockInfo> entities = new HashMap<>();
        entities.put("player", InternalEntity.PLAYER.getInfo());
        entities.put("zombie", InternalEntity.ZOMBIE.getInfo());

        INEntityRegistry.initVanillaEntities(entities);


        INModUtil.initModMapper(mod -> aliasesMods.get(mod).getAnnotation());
        INTagUtil.initEmptyTagFactory(InternalTagCompound::new);
        if (event.getSide().isClient())
            MinecraftForge.EVENT_BUS.register(new ClientInternalHandler());
        MinecraftForge.EVENT_BUS.register(this);
        Set<ASMDataTable.ASMData> mods = event.getAsmData().getAll(Modification.class.getName());
        mods.forEach(data -> {
            try {
                Class<?> mod = Class.forName(data.getClassName());
                Modification modInfo = mod.getDeclaredAnnotation(Modification.class);
                final String modid = modInfo.value();

                Object modObj = mod.newInstance();
                if (IMod.class.isAssignableFrom(mod)) {
                    IMod imod = (IMod) modObj;
                    VersatileMod.mods.put(modid, new ModStorage(modInfo, imod));
                    VersatileMod.aliasesMods.put(imod, new ModStorage(modInfo, imod));
                } else {
                    throw new RuntimeException(mod.getName() + " is not IMod!");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        if (FMLCommonHandler.instance().getSide().isClient())
            ClientInternalHandler.load();

        MinecraftForge.EVENT_BUS.register(new BlockHandlers());
        MinecraftForge.EVENT_BUS.register(new EntityHandlers());


    }

    private static void registerBlockHandler(BlockHandler handler, BlockInfo info) {
        List<BlockHandler> handlers = blockHandlerMap.computeIfAbsent(info, k -> new ArrayList<>());
        handlers.add(handler);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        BlockHandlerInitializer initializer = VersatileMod::registerBlockHandler;

        for (ModStorage storage : mods.values())
            storage.getMod().createBlockHandlers(initializer);


        EntityHandlerInitializer entityInitializer = (info, handler) -> {
            List<EntityHandler<?>> handlers = entityHandlerMap.computeIfAbsent(info, k -> new ArrayList<>());
            handlers.add(handler);
        };

        for (ModStorage storage : mods.values())
            storage.getMod().createEntityHandlers(entityInitializer);

        EntityCache.bake(entityHandlerMap);
        BlockHandlers.initHandlers(getBlockHandlerMap(), blockDataHandlerMap);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        GameRegistry.registerTileEntity(TileEntityDataBlock.class, new ResourceLocation(MODID, "data_block_capacitor"));
        GameRegistry.registerTileEntity(TileEntityDataBlockUpdatable.class, new ResourceLocation(MODID, "data_block_capacitor_updatable"));
    }

    @SubscribeEvent
    public void registerBlocks(RegistryEvent.Register<Block> event) {
        for (ModStorage storage : mods.values()) {
            BlockList<?> blocks = new BlockList<>();
            BlockInitializer initializer = new BlockInitializer() {
                @SuppressWarnings("all")
                private boolean canModRegister(BlockInfo info) {
                    return storage.getModid().equals(info.getOwner());
                }

                private void validate(BlockInfo source) {
                    if (source == null)
                        throw new RuntimeException("Block source not specified!");
                    if (!canModRegister(source))
                        throw new RuntimeException("Invalid source owner!");
                }

                @Override
                public BlockInfo registerBlock(String uniqueName, Consumer<BlockFactory> factory) {
                    BlockInfo source = new BlockInfo(storage.getModid(), uniqueName);
                    registerBlock(source, factory);
                    return source;
                }

                @Override
                public void registerBlock(BlockInfo source, Consumer<BlockFactory> factory) {
                    validate(source);

                    BlockInternalFactory internalFactory = new BlockInternalFactory(source);
                    factory.accept(internalFactory);

                    BlockInternalWrapper wrapper = new BlockInternalWrapper(source, internalFactory);
                    blocks.add(wrapper);
                    event.getRegistry().register(wrapper);

                    if (blockInfoMap.containsKey(source))
                        throw new RuntimeException("Id \"" + source.toString() + "\" already registered!");
                    blockInfoMap.put(BlockCache.add(wrapper, source), wrapper);
                }

                @Override
                public BlockInfo registerDataBlock(
                        String uniqueName,
                        Consumer<BlockFactory> factory,
                        Supplier<DataCapacitor> allocator
                ) {
                    BlockInfo source = new BlockInfo(storage.getModid(), uniqueName);
                    registerDataBlock(source, factory, allocator);
                    return source;
                }

                @Override
                public void registerDataBlock(
                        BlockInfo source,
                        Consumer<BlockFactory> factory,
                        Supplier<DataCapacitor> allocator
                ) {
                    validate(source);

                    BlockInternalFactory internalFactory = new BlockInternalFactory(source);
                    factory.accept(internalFactory);

                    DataCapacitor capacitor = allocator.get();
                    if (capacitor instanceof BlockHandler)
                        blockDataHandlerMap.add(((BlockHandler) capacitor).getClass());
                    boolean isUpdatable = capacitor instanceof Updatable;



                    BlockTileInternalWrapper wrapper = new BlockTileInternalWrapper(
                            source,
                            internalFactory,
                            isUpdatable, allocator
                    );

                    blocks.add(wrapper);
                    event.getRegistry().register(wrapper);

                    if (blockInfoMap.containsKey(source))
                        throw new RuntimeException("Id \"" + source.toString() + "\" already registered!");
                    blockInfoMap.put(BlockCache.add(wrapper, source), wrapper);

                }
            };

            storage.getMod().createBlocks(initializer);
            VersatileMod.blocks.put(storage.getModid(), blocks);
        }

    }

    @SubscribeEvent
    public void registerItems(RegistryEvent.Register<Item> event) {
        for (ModStorage storage : mods.values()) {
            ItemList<?> items = new ItemList<>();
            ItemInitializer initializer = new ItemInitializer() {
                @SuppressWarnings("all")
                private boolean canModRegister(BlockInfo info) {
                    return storage.getModid().equals(info.getOwner());
                }

                private void validate(BlockInfo source) {
                    if (source == null)
                        throw new RuntimeException("Item source not specified!");
                    if (!canModRegister(source))
                        throw new RuntimeException("Invalid source owner!");
                }

                @Override
                public void registerItem(BlockInfo source, Consumer<ItemFactory> factoryConsumer) {
                    validate(source);
                    ItemInternalFactory internalFactory = new ItemInternalFactory(source);
                    factoryConsumer.accept(internalFactory);
                    ItemInternalWrapper wrapper = new ItemInternalWrapper(source, internalFactory);
                    items.add(wrapper);
                    event.getRegistry().register(wrapper);
                }

                @Override
                public BlockInfo registerItem(String uniqueName, Consumer<ItemFactory> factory) {
                    BlockInfo source = new BlockInfo(storage.getModid(), uniqueName);
                    registerItem(source, factory);
                    return source;
                }
            };


            blocks.get(storage.getModid()).forEach(wrapper -> {
                IBlock iBlock = wrapper.get();
                if (iBlock.isCreateItemBlock()) {
                    ItemBlockInternalWrapper itemBlock = new ItemBlockInternalWrapper(iBlock.getSource(), wrapper,
                            new IItemBlockInternalWrapper(iBlock));
                    items.add(itemBlock);
                    event.getRegistry().register(itemBlock);
                }
            });

            storage.getMod().createItems(initializer);
            VersatileMod.items.put(storage.getModid(), items);
        }
    }

    @SuppressWarnings("all")
    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void guiMod(TickEvent.ClientTickEvent event) throws NoSuchFieldException, IllegalAccessException {
        if (isModsInit) return;
        isModsInit = true;

        Loader loader = Loader.instance();

        Field mods = Loader.class.getDeclaredField("mods");
        mods.setAccessible(true);

        Field namedMods = Loader.class.getDeclaredField("namedMods");
        namedMods.setAccessible(true);

        Field modController = Loader.class.getDeclaredField("modController");
        modController.setAccessible(true);

        Field eventChannels = LoadController.class.getDeclaredField("eventChannels");
        eventChannels.setAccessible(true);

        LoadController loadController = (LoadController) modController.get(loader);


        HashMap<String, EventBus> temporary = Maps.newHashMap((Map<String, EventBus>) eventChannels.get(loadController));
        HashMap<String, ModContainer> namedModsTemp = Maps.newHashMap((Map<String, ModContainer>) namedMods.get(loader));

        List<ModContainer> containerList = (List<ModContainer>) mods.get(loader);


        ModContainer versatileContainer = null;

        for (ModContainer container : containerList) {
            if (container.getModId().equals(MODID))
                versatileContainer = container;
        }

        InfoEnvironment environment = new InfoDispatcher();
        ModContainer finalVersatileContainer = versatileContainer;
        List<VModContainer> list = VersatileMod.mods.values().stream()
                .map(storage -> new VModContainer(storage.getModid(), storage.getMod(), environment, finalVersatileContainer))
                .collect(Collectors.toList());


        for (VModContainer container : list) {
            EventBus bus = new FMLThrowingEventBus((exception, context) -> loadController.errorOccurred(container, exception));
            temporary.put(container.getModId(), bus);
            namedModsTemp.put(container.getModId(), container);
        }


        List<ModContainer> newContainers = new ArrayList<>(containerList);
        newContainers.addAll(list);

        eventChannels.set(loadController, ImmutableMap.copyOf(temporary));
        mods.set(loader, newContainers);
        namedMods.set(loader, namedModsTemp);


        loader.getActiveModList().addAll(list);
    }
}
