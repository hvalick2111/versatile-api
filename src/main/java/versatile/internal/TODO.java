package versatile.internal;

public class TODO extends RuntimeException {
    public TODO(String message) {
        super("Implementation does not exits: "+message);
    }

    public TODO() {
        super("Implementation does not exits");
    }
}