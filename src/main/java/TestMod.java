import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

//@Mod(modid = "testmod")
public class TestMod {

    public static final SimpleNetworkWrapper NETWORK = NetworkRegistry.INSTANCE.newSimpleChannel("testmod");


    private static short id;


    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        NETWORK.registerMessage(MessageUpdate.class,MessageUpdate.class, id++, Side.CLIENT);
        MinecraftForge.EVENT_BUS.register(this);
    }


    @SubscribeEvent
    public void event(PlayerInteractEvent event) {
        if (!event.getWorld().isRemote) {
            NETWORK.sendTo(new MessageUpdate(event.getEntityPlayer().getEntityId(), true), (EntityPlayerMP) event.getEntityPlayer());
        }
    }
}