import java.lang.reflect.Array;
import java.util.Objects;

public class test {
    private static <T> T[] getArr(T val){

        @SuppressWarnings("unchecked")
        T[] arr =  (T[]) Array.newInstance(val.getClass(), 0);
        return arr;
    }


    static class Node {
        private Node next;

        private Object value;

        void add(Object value){
            Objects.requireNonNull(value);
            if(this.value == null)
                this.value = value;
            else {
                if(next == null) next = new Node();
                next.add(value);
            }
        }

        int size(){
            return value == null ? 0 : 1 + (next == null ? 0 :next.size());
        }
    }

//    static {
//        Node var = new Node();
//        var.add("test");
//        var.add("test2");
//
//        System.out.println(var.size());
//    }




//


    public static void main(String[] args) {



//        System.out.println();
//
//        Consumer<String> as = (Consumer<String> & Cloneable) a -> {};
//        System.out.println(as.getClass().getName());

        String[] lines = str.split("\n");
        for (String line: lines){
            line = line.trim();
            if(line.contains("//")) continue;
            String[] words = line.split("\\s");
            String name = words[4].toLowerCase().replace(";", "");
            System.out.println("blocks.put(\""+name.toLowerCase() + "\", ofAdd(Blocks."+ name.toUpperCase()+"));");
        }

//        Object o1 = new Object();
//        test(Object::new);
//
//
//
//
//        HashBiMap<String, Integer> hashBiMap = HashBiMap.create(10000);
//        BiMap<Integer, String> test = hashBiMap.inverse();
//
//        for (int i = 0; i < 1000; i++) {
//            int random = ThreadLocalRandom.current().nextInt(10000);
//            String randomStr = random + "ed";
//            hashBiMap.put(randomStr, random);
//            System.out.println(randomStr + " : " + random);
//        }
//
//        System.out.println();
//        System.out.println();
//        System.out.println();
//
//        {
//
//            long start =  System.nanoTime();
//            hashBiMap.get("1ed");
//            long end =  System.nanoTime();
//            System.out.println();
//            System.out.println();
//            System.out.println("PreLoad: " + (end - start) / 1_000_000F);
//        }
//
//
//
//        Scanner scanner = new Scanner(System.in);
//
//        long start =  System.nanoTime();
//        StringBuilder builder = new StringBuilder();
//        for (int i = 0; i < 10_000; i++) {
//            int random = ThreadLocalRandom.current().nextInt(1000);
//            String str = hashBiMap.inverse().get(random);
//            builder.append(str == null ? "" : str);
//            System.out.println(random + " : " + str);
//        }
//        long end =  System.nanoTime();
//        System.out.println(builder.toString().charAt(scanner.nextInt()));
//        System.out.println(builder.length());
//        System.out.println();
//        System.out.println();
//        System.out.println((end - start) / 1_000_000F);
    }

    public static String str = "public static final Block STONE;\n" +
            "public static final BlockGrass GRASS;\n" +
            "public static final Block DIRT;\n" +
            "public static final Block COBBLESTONE;\n" +
            "public static final Block PLANKS;\n" +
            "public static final Block SAPLING;\n" +
            "public static final Block BEDROCK;\n" +
            "public static final BlockDynamicLiquid FLOWING_WATER;\n" +
            "public static final BlockStaticLiquid WATER;\n" +
            "public static final BlockDynamicLiquid FLOWING_LAVA;\n" +
            "public static final BlockStaticLiquid LAVA;\n" +
            "public static final BlockSand SAND;\n" +
            "public static final Block GRAVEL;\n" +
            "public static final Block GOLD_ORE;\n" +
            "public static final Block IRON_ORE;\n" +
            "public static final Block COAL_ORE;\n" +
            "public static final Block LOG;\n" +
            "public static final Block LOG2;\n" +
            "public static final BlockLeaves LEAVES;\n" +
            "public static final BlockLeaves LEAVES2;\n" +
            "public static final Block SPONGE;\n" +
            "public static final Block GLASS;\n" +
            "public static final Block LAPIS_ORE;\n" +
            "public static final Block LAPIS_BLOCK;\n" +
            "public static final Block DISPENSER;\n" +
            "public static final Block SANDSTONE;\n" +
            "public static final Block NOTEBLOCK;\n" +
            "public static final Block BED;\n" +
            "public static final Block GOLDEN_RAIL;\n" +
            "public static final Block DETECTOR_RAIL;\n" +
            "public static final BlockPistonBase STICKY_PISTON;\n" +
            "public static final Block WEB;\n" +
            "public static final BlockTallGrass TALLGRASS;\n" +
            "public static final BlockDeadBush DEADBUSH;\n" +
            "public static final BlockPistonBase PISTON;\n" +
            "public static final BlockPistonExtension PISTON_HEAD;\n" +
            "public static final Block WOOL;\n" +
            "public static final BlockPistonMoving PISTON_EXTENSION;\n" +
            "public static final BlockFlower YELLOW_FLOWER;\n" +
            "public static final BlockFlower RED_FLOWER;\n" +
            "public static final BlockBush BROWN_MUSHROOM;\n" +
            "public static final BlockBush RED_MUSHROOM;\n" +
            "public static final Block GOLD_BLOCK;\n" +
            "public static final Block IRON_BLOCK;\n" +
            "public static final BlockSlab DOUBLE_STONE_SLAB;\n" +
            "public static final BlockSlab STONE_SLAB;\n" +
            "public static final Block BRICK_BLOCK;\n" +
            "public static final Block TNT;\n" +
            "public static final Block BOOKSHELF;\n" +
            "public static final Block MOSSY_COBBLESTONE;\n" +
            "public static final Block OBSIDIAN;\n" +
            "public static final Block TORCH;\n" +
            "public static final BlockFire FIRE;\n" +
            "public static final Block MOB_SPAWNER;\n" +
            "public static final Block OAK_STAIRS;\n" +
            "public static final BlockChest CHEST;\n" +
            "public static final BlockRedstoneWire REDSTONE_WIRE;\n" +
            "public static final Block DIAMOND_ORE;\n" +
            "public static final Block DIAMOND_BLOCK;\n" +
            "public static final Block CRAFTING_TABLE;\n" +
            "public static final Block WHEAT;\n" +
            "public static final Block FARMLAND;\n" +
            "public static final Block FURNACE;\n" +
            "public static final Block LIT_FURNACE;\n" +
            "public static final Block STANDING_SIGN;\n" +
            "public static final BlockDoor OAK_DOOR;\n" +
            "public static final BlockDoor SPRUCE_DOOR;\n" +
            "public static final BlockDoor BIRCH_DOOR;\n" +
            "public static final BlockDoor JUNGLE_DOOR;\n" +
            "public static final BlockDoor ACACIA_DOOR;\n" +
            "public static final BlockDoor DARK_OAK_DOOR;\n" +
            "public static final Block LADDER;\n" +
            "public static final Block RAIL;\n" +
            "public static final Block STONE_STAIRS;\n" +
            "public static final Block WALL_SIGN;\n" +
            "public static final Block LEVER;\n" +
            "public static final Block STONE_PRESSURE_PLATE;\n" +
            "public static final BlockDoor IRON_DOOR;\n" +
            "public static final Block WOODEN_PRESSURE_PLATE;\n" +
            "public static final Block REDSTONE_ORE;\n" +
            "public static final Block LIT_REDSTONE_ORE;\n" +
            "public static final Block UNLIT_REDSTONE_TORCH;\n" +
            "public static final Block REDSTONE_TORCH;\n" +
            "public static final Block STONE_BUTTON;\n" +
            "public static final Block SNOW_LAYER;\n" +
            "public static final Block ICE;\n" +
            "public static final Block SNOW;\n" +
            "public static final BlockCactus CACTUS;\n" +
            "public static final Block CLAY;\n" +
            "public static final BlockReed REEDS;\n" +
            "public static final Block JUKEBOX;\n" +
            "public static final Block OAK_FENCE;\n" +
            "public static final Block SPRUCE_FENCE;\n" +
            "public static final Block BIRCH_FENCE;\n" +
            "public static final Block JUNGLE_FENCE;\n" +
            "public static final Block DARK_OAK_FENCE;\n" +
            "public static final Block ACACIA_FENCE;\n" +
            "public static final Block PUMPKIN;\n" +
            "public static final Block NETHERRACK;\n" +
            "public static final Block SOUL_SAND;\n" +
            "public static final Block GLOWSTONE;\n" +
            "public static final BlockPortal PORTAL;\n" +
            "public static final Block LIT_PUMPKIN;\n" +
            "public static final Block CAKE;\n" +
            "public static final BlockRedstoneRepeater UNPOWERED_REPEATER;\n" +
            "public static final BlockRedstoneRepeater POWERED_REPEATER;\n" +
            "public static final Block TRAPDOOR;\n" +
            "public static final Block MONSTER_EGG;\n" +
            "public static final Block STONEBRICK;\n" +
            "public static final Block BROWN_MUSHROOM_BLOCK;\n" +
            "public static final Block RED_MUSHROOM_BLOCK;\n" +
            "public static final Block IRON_BARS;\n" +
            "public static final Block GLASS_PANE;\n" +
            "public static final Block MELON_BLOCK;\n" +
            "public static final Block PUMPKIN_STEM;\n" +
            "public static final Block MELON_STEM;\n" +
            "public static final Block VINE;\n" +
            "public static final Block OAK_FENCE_GATE;\n" +
            "public static final Block SPRUCE_FENCE_GATE;\n" +
            "public static final Block BIRCH_FENCE_GATE;\n" +
            "public static final Block JUNGLE_FENCE_GATE;\n" +
            "public static final Block DARK_OAK_FENCE_GATE;\n" +
            "public static final Block ACACIA_FENCE_GATE;\n" +
            "public static final Block BRICK_STAIRS;\n" +
            "public static final Block STONE_BRICK_STAIRS;\n" +
            "public static final BlockMycelium MYCELIUM;\n" +
            "public static final Block WATERLILY;\n" +
            "public static final Block NETHER_BRICK;\n" +
            "public static final Block NETHER_BRICK_FENCE;\n" +
            "public static final Block NETHER_BRICK_STAIRS;\n" +
            "public static final Block NETHER_WART;\n" +
            "public static final Block ENCHANTING_TABLE;\n" +
            "public static final Block BREWING_STAND;\n" +
            "public static final BlockCauldron CAULDRON;\n" +
            "public static final Block END_PORTAL;\n" +
            "public static final Block END_PORTAL_FRAME;\n" +
            "public static final Block END_STONE;\n" +
            "public static final Block DRAGON_EGG;\n" +
            "public static final Block REDSTONE_LAMP;\n" +
            "public static final Block LIT_REDSTONE_LAMP;\n" +
            "public static final BlockSlab DOUBLE_WOODEN_SLAB;\n" +
            "public static final BlockSlab WOODEN_SLAB;\n" +
            "public static final Block COCOA;\n" +
            "public static final Block SANDSTONE_STAIRS;\n" +
            "public static final Block EMERALD_ORE;\n" +
            "public static final Block ENDER_CHEST;\n" +
            "public static final BlockTripWireHook TRIPWIRE_HOOK;\n" +
            "public static final Block TRIPWIRE;\n" +
            "public static final Block EMERALD_BLOCK;\n" +
            "public static final Block SPRUCE_STAIRS;\n" +
            "public static final Block BIRCH_STAIRS;\n" +
            "public static final Block JUNGLE_STAIRS;\n" +
            "public static final Block COMMAND_BLOCK;\n" +
            "public static final BlockBeacon BEACON;\n" +
            "public static final Block COBBLESTONE_WALL;\n" +
            "public static final Block FLOWER_POT;\n" +
            "public static final Block CARROTS;\n" +
            "public static final Block POTATOES;\n" +
            "public static final Block WOODEN_BUTTON;\n" +
            "public static final BlockSkull SKULL;\n" +
            "public static final Block ANVIL;\n" +
            "public static final Block TRAPPED_CHEST;\n" +
            "public static final Block LIGHT_WEIGHTED_PRESSURE_PLATE;\n" +
            "public static final Block HEAVY_WEIGHTED_PRESSURE_PLATE;\n" +
            "public static final BlockRedstoneComparator UNPOWERED_COMPARATOR;\n" +
            "public static final BlockRedstoneComparator POWERED_COMPARATOR;\n" +
            "public static final BlockDaylightDetector DAYLIGHT_DETECTOR;\n" +
            "public static final BlockDaylightDetector DAYLIGHT_DETECTOR_INVERTED;\n" +
            "public static final Block REDSTONE_BLOCK;\n" +
            "public static final Block QUARTZ_ORE;\n" +
            "public static final BlockHopper HOPPER;\n" +
            "public static final Block QUARTZ_BLOCK;\n" +
            "public static final Block QUARTZ_STAIRS;\n" +
            "public static final Block ACTIVATOR_RAIL;\n" +
            "public static final Block DROPPER;\n" +
            "public static final Block STAINED_HARDENED_CLAY;\n" +
            "public static final Block BARRIER;\n" +
            "public static final Block IRON_TRAPDOOR;\n" +
            "public static final Block HAY_BLOCK;\n" +
            "public static final Block CARPET;\n" +
            "public static final Block HARDENED_CLAY;\n" +
            "public static final Block COAL_BLOCK;\n" +
            "public static final Block PACKED_ICE;\n" +
            "public static final Block ACACIA_STAIRS;\n" +
            "public static final Block DARK_OAK_STAIRS;\n" +
            "public static final Block SLIME_BLOCK;\n" +
            "public static final BlockDoublePlant DOUBLE_PLANT;\n" +
            "public static final BlockStainedGlass STAINED_GLASS;\n" +
            "public static final BlockStainedGlassPane STAINED_GLASS_PANE;\n" +
            "public static final Block PRISMARINE;\n" +
            "public static final Block SEA_LANTERN;\n" +
            "public static final Block STANDING_BANNER;\n" +
            "public static final Block WALL_BANNER;\n" +
            "public static final Block RED_SANDSTONE;\n" +
            "public static final Block RED_SANDSTONE_STAIRS;\n" +
            "public static final BlockSlab DOUBLE_STONE_SLAB2;\n" +
            "public static final BlockSlab STONE_SLAB2;\n" +
            "public static final Block END_ROD;\n" +
            "public static final Block CHORUS_PLANT;\n" +
            "public static final Block CHORUS_FLOWER;\n" +
            "public static final Block PURPUR_BLOCK;\n" +
            "public static final Block PURPUR_PILLAR;\n" +
            "public static final Block PURPUR_STAIRS;\n" +
            "public static final BlockSlab PURPUR_DOUBLE_SLAB;\n" +
            "public static final BlockSlab PURPUR_SLAB;\n" +
            "public static final Block END_BRICKS;\n" +
            "public static final Block BEETROOTS;\n" +
            "public static final Block GRASS_PATH;\n" +
            "public static final Block END_GATEWAY;\n" +
            "public static final Block REPEATING_COMMAND_BLOCK;\n" +
            "public static final Block CHAIN_COMMAND_BLOCK;\n" +
            "public static final Block FROSTED_ICE;\n" +
            "public static final Block MAGMA;\n" +
            "public static final Block NETHER_WART_BLOCK;\n" +
            "public static final Block RED_NETHER_BRICK;\n" +
            "public static final Block BONE_BLOCK;\n" +
            "public static final Block STRUCTURE_VOID;\n" +
            "public static final Block OBSERVER;\n" +
            "public static final Block WHITE_SHULKER_BOX;\n" +
            "public static final Block ORANGE_SHULKER_BOX;\n" +
            "public static final Block MAGENTA_SHULKER_BOX;\n" +
            "public static final Block LIGHT_BLUE_SHULKER_BOX;\n" +
            "public static final Block YELLOW_SHULKER_BOX;\n" +
            "public static final Block LIME_SHULKER_BOX;\n" +
            "public static final Block PINK_SHULKER_BOX;\n" +
            "public static final Block GRAY_SHULKER_BOX;\n" +
            "public static final Block SILVER_SHULKER_BOX;\n" +
            "public static final Block CYAN_SHULKER_BOX;\n" +
            "public static final Block PURPLE_SHULKER_BOX;\n" +
            "public static final Block BLUE_SHULKER_BOX;\n" +
            "public static final Block BROWN_SHULKER_BOX;\n" +
            "public static final Block GREEN_SHULKER_BOX;\n" +
            "public static final Block RED_SHULKER_BOX;\n" +
            "public static final Block BLACK_SHULKER_BOX;\n" +
            "public static final Block WHITE_GLAZED_TERRACOTTA;\n" +
            "public static final Block ORANGE_GLAZED_TERRACOTTA;\n" +
            "public static final Block MAGENTA_GLAZED_TERRACOTTA;\n" +
            "public static final Block LIGHT_BLUE_GLAZED_TERRACOTTA;\n" +
            "public static final Block YELLOW_GLAZED_TERRACOTTA;\n" +
            "public static final Block LIME_GLAZED_TERRACOTTA;\n" +
            "public static final Block PINK_GLAZED_TERRACOTTA;\n" +
            "public static final Block GRAY_GLAZED_TERRACOTTA;\n" +
            "public static final Block SILVER_GLAZED_TERRACOTTA;\n" +
            "public static final Block CYAN_GLAZED_TERRACOTTA;\n" +
            "public static final Block PURPLE_GLAZED_TERRACOTTA;\n" +
            "public static final Block BLUE_GLAZED_TERRACOTTA;\n" +
            "public static final Block BROWN_GLAZED_TERRACOTTA;\n" +
            "public static final Block GREEN_GLAZED_TERRACOTTA;\n" +
            "public static final Block RED_GLAZED_TERRACOTTA;\n" +
            "public static final Block BLACK_GLAZED_TERRACOTTA;\n" +
            "public static final Block CONCRETE;\n" +
            "public static final Block CONCRETE_POWDER;\n" +
            "public static final Block STRUCTURE_BLOCK;";
}
