package com.example.examplemod;

import versatile.api.IMod;
import versatile.api.Modification;
import versatile.api.game.GameSide;
import versatile.api.game.block.BlockHandler;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.block.BlockInitializer;
import versatile.api.game.entity.Hand;
import versatile.api.game.entity.world.PlayerContainer;
import versatile.api.game.gui.SlotGroup;
import versatile.api.game.inventory.ArrayInventory;
import versatile.api.game.item.ItemContainer;
import versatile.api.game.item.property.WoodType;
import versatile.api.game.world.Pos;
import versatile.api.game.world.WorldController;

import java.net.URL;

@Modification("example")
public class TestMod implements IMod {
    public static final String URL = "https://sun9-13.userapi.com/c854020/v854020855/caf7e/EJF_ravVaoc.jpg";

    @Override
    public void createBlocks(BlockInitializer initializer) {
        initializer.registerDataBlock(new BlockInfo(this, "test"), factory -> {
            factory.setDisplayName(l -> l.permanent("Test"));
            factory.setTexture(manager -> manager.loadTextureFromURL(new URL(URL)));
        }, TestData::new);
    }


    private static class TestData implements ArrayInventory, BlockHandler{
        private final ItemContainer[] container = new ItemContainer[27];
        private final SlotGroup mainBar = new SlotGroup(this).subGroup(0, 9);
        private final SlotGroup otherBar = new SlotGroup(this).subGroup(9, 27);

        @Override
        public ItemContainer[] getItems() {
            return container;
        }

        @Override
        public boolean blockActivated(WorldController world, Pos pos, PlayerContainer clicker, Hand clickHand) {

//            setItem(0, new ItemContainer(VanillaItem.BONE_MEAL, 64));
//            setItem(1, new ItemContainer(VanillaItem.INK_SAC, 64));
//            setItem(2, new ItemContainer(VanillaItem.COCOA_BEANS, 64));
//            setItem(3, new ItemContainer(VanillaItem.LAPIS_LAZULI, 64));

            clicker.openInventory(this, gui -> {
                gui.makeLines(9);
                gui.overrideSlotsInfo(mainBar, slot -> {
                    slot.setMask(item -> item.hasProperty(WoodType.JUNGLE));
                });
            }, GameSide.SERVER);

//            clicker.openInventory(() -> this);
            return true;
        }
    }
}
