package com.example.examplemod.examples;

import versatile.api.IMod;
import versatile.api.Modification;
import versatile.api.game.block.BlockInitializer;
import versatile.api.game.block.HarvestType;

@Modification("example_blocks2")
public class ExampleModBlocks2 implements IMod {
    @Override
    public void createBlocks(BlockInitializer initializer) {
        initializer.registerBlock("copper_ore", factory -> {
           factory.setDisplayName(manager -> manager.fromLangProperties("block.copper_ore"));
           factory.setTexture(textureFactory -> textureFactory.loadTextureFromResource("copper_ore"));
           factory.setHardness(1.6f);
           factory.setHarvestTier(1);
           factory.setHarvestType(HarvestType.PICKAXE);
        });

        initializer.registerBlock("tin_ore", factory -> {
            factory.setDisplayName(manager -> manager.fromLangProperties("block.tin_ore"));
            factory.setTexture(textureFactory -> textureFactory.loadTextureFromResource("tin_ore"));
            factory.setHardness(1.8f);
            factory.setHarvestTier(1);
            factory.setHarvestType(HarvestType.PICKAXE);
        });
    }
}
