package com.example.examplemod.examples;

import versatile.api.IMod;
import versatile.api.Modification;
import versatile.api.game.OptionalBlockInfo;
import versatile.api.game.block.BlockInfo;
import versatile.api.game.block.BlockInitializer;
import versatile.api.game.block.BlockMaterial;
import versatile.api.game.block.HarvestType;
import versatile.api.game.lang.IDisplayName;

import javax.annotation.Nullable;
import java.net.URL;

@Modification("example_blocks")
public class ExampleModBlocks implements IMod {
    @Override
    public void createBlocks(BlockInitializer initializer) {
        Blocks.register(initializer);
    }

    enum Blocks implements OptionalBlockInfo {
        THE_CURSE_OF_THE_SEAS(
                "the_curse_of_the_seas",
                "https://avatars.mds.yandex.net/get-pdb/2004852/87c125fe-cffa-4f5d-9921-0e4f54259a41/s1200",
                manager -> manager.permanent("Проклятье морей")
        ),
        LEGEND_TREASURE(
                "legend_treasure",
                "https://metal.academy/uploads/releases/0864ebe1f30512af79295f37d26d6d81.jpg",
                manager -> manager.permanent("Сокровище Энии")
        ),
        ;

        private final String name;
        private final String textureUrl;
        private final @Nullable IDisplayName displayName;

        public BlockInfo tempInfo;

        Blocks(String name, String textureUrl, IDisplayName displayName) {
            this.name = name;
            this.textureUrl = textureUrl;
            this.displayName = displayName;
        }

        @Override
        public BlockInfo orEmpty() {
            if (tempInfo == null)
                return BlockInfo.EMPTY;
            return tempInfo;
        }

        public static void register(BlockInitializer initializer) {
            for (Blocks block : Blocks.values()) {
                block.tempInfo = initializer.registerBlock(block.name, factory -> {
                    if (block.displayName == null)
                        factory.setDisplayName(manager -> manager.permanentFormatted(block.name));
                    else
                        factory.setDisplayName(block.displayName);
                    factory.setTexture(texture -> texture.loadTextureFromURL(new URL(block.textureUrl)));

                    factory.setHardness(0.2f);
                    factory.setHarvestType(HarvestType.PICKAXE);
                    factory.setHarvestTier(2);

                    factory.setMaterial(BlockMaterial.ROCK);
                });
            }
        }
    }
}
