package com.example.examplemod.examples;

import versatile.api.IMod;
import versatile.api.Modification;
import versatile.api.game.item.ItemInitializer;

import java.awt.*;

@Modification("example_items")
public class ExampleModItems implements IMod {
    @Override
    public void createItems(ItemInitializer initializer) {
        Color[] colors = {Color.GRAY, Color.GREEN, Color.WHITE, Color.RED, Color.BLACK, Color.BLUE, Color.YELLOW};

        for (int i = 0; i < colors.length; i++) {
            Color color = colors[i];
            initializer.registerItem("simple_color_" + i, factory -> {
                factory.setDisplayName(manager -> manager.permanentFormatted(color.toString()));
                factory.setTexture(manager -> manager.loadTextureFromColor(color));
            });

        }
    }
}
