package versatile.test;

import net.minecraft.client.Minecraft;

import java.lang.reflect.Method;

public class MinecraftTester {

    public static void main(String[] args) throws Throwable {
        Class<?> gradleStart = Class.forName("GradleStart");
        Method main = gradleStart.getMethod("main", String[].class);
        main.invoke(null, (Object) new String[0]);
    }

    public static void prepare() {
        VersatileTestRunner.prepare();
    }

    static void stop() {
        Minecraft.getMinecraft().addScheduledTask(() -> Minecraft.getMinecraft().shutdown());
    }
}
