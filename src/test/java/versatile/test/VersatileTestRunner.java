package versatile.test;

import net.minecraft.client.Minecraft;
import net.minecraft.launchwrapper.Launch;
import org.jetbrains.annotations.NotNull;
import org.junit.internal.runners.model.ReflectiveCallable;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;
import org.junit.runners.model.TestClass;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class VersatileTestRunner extends CleaningTestRunner {
    private static volatile boolean launched = false;

    /**
     * Creates a BlockJUnit4ClassRunner to run {@code klass}
     *
     * @param klass
     * @throws InitializationError if the test class is malformed.
     */
    public VersatileTestRunner(Class<?> klass) throws InitializationError {
        super(getFromTestClassloader(klass));
    }


    public static CountDownLatch latch = new CountDownLatch(1);

    static void prepare() {
        try {
            while (!latch.await(5, TimeUnit.SECONDS)) {
                System.err.println("Waiting main thread...");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static <T> CompletableFuture<T> computeTask(Callable<T> callable) {
        CompletableFuture<T> future = new CompletableFuture<>();
        Minecraft.getMinecraft().addScheduledTask(() -> {
            try {
                future.complete(callable.call());
            } catch (Throwable e) {
                future.completeExceptionally(e);
            }
        });
        return future;
    }

    @SuppressWarnings("unused") //used in reflection
    public static <T> void testTask(Runnable callable) {
        try {
            CompletableFuture<Object> future = computeTask(Executors.callable(callable));
            while (true) {
                try {
                    future.get(10, TimeUnit.SECONDS);
                    break;
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    private static Class<?> getFromTestClassloader(Class<?> clazz) throws InitializationError {
        try {
            runMinecraftAndWait();
            ClassLoader testClassLoader = Launch.classLoader;
            return Class.forName(clazz.getName(), true, testClassLoader);
        } catch (ClassNotFoundException e) {
            throw new InitializationError(e);
        }
    }

    @NotNull
    private List<FrameworkMethod> getCollect(TestClass testClass, Class<? extends Annotation> pattern) {
        return testClass.getAnnotatedMethods().stream().filter(it -> {
            List<Annotation> stream = Arrays.asList(it.getAnnotations());
            return stream.stream().anyMatch(annotation -> annotation.annotationType().getName().equals(pattern.getName()));
        }).collect(Collectors.toList());
    }

    @Override
    protected TestClass createTestClass(Class<?> testClass) {
        return new TestClass(testClass) {
            @Override
            public List<FrameworkMethod> getAnnotatedMethods(Class<? extends Annotation> annotationClass) {
                return getCollect(this, annotationClass);
            }
        };
    }

    @Override
    protected Statement methodInvoker(FrameworkMethod method, Object test) {
        return new InvokeRedirectMethod(method, test);
    }

    private static synchronized void runMinecraftAndWait() {
        if (!launched) {
            launched = true;
            Thread thread = new Thread(() -> {
                try {
                    MinecraftTester.main(new String[0]);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            });
            thread.start();
        }


        while (true) {
            if (waitClassLoader())
                break;
        }
    }

    private static boolean waitClassLoader() {
        if (Launch.classLoader == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return false;
        }
        return true;
    }

    public static class InvokeRedirectMethod extends Statement {
        private final FrameworkMethod testMethod;
        private final Object target;

        public InvokeRedirectMethod(FrameworkMethod testMethod, Object target) {
            this.testMethod = testMethod;
            this.target = target;
        }

        @Override
        public void evaluate() throws Throwable {
            new ReflectiveCallable() {
                @Override
                protected Object runReflectiveCall() throws Throwable {
                    Method method = testMethod.getMethod();
                    Class<?> clazz = Class.forName(
                            VersatileTestRunner.class.getName(),
                            true,
                            Launch.classLoader
                    );
                    Method testTask = clazz.getMethod("testTask", Runnable.class);
                    Object invoked;
                    try {
                        invoked = testTask.invoke(null, (Runnable) (() -> {
                            try {
                                method.invoke(target);
                            } catch (IllegalAccessException | InvocationTargetException e) {
                                throw new RuntimeException(e);
                            }
                        }));

                    } catch (Throwable e) {
                        while (e.getCause() != null) {
                            e = e.getCause();
                        }
                        throw e;
                    }
                    return invoked;
                }
            }.run();
        }
    }

    @Override
    protected void cleanupAfterAllTestRuns() {
        try {
            Class<?> clazz = Class.forName(MinecraftTester.class.getName(), true, Launch.classLoader);
            Method stop = clazz.getDeclaredMethod("stop");
            stop.setAccessible(true);
            stop.invoke(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}