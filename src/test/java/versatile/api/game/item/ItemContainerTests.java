package versatile.api.game.item;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import versatile.api.game.block.data.TagCompound;
import versatile.api.game.item.property.ColorType;
import versatile.api.game.item.property.WoodType;
import versatile.test.MinecraftTester;
import versatile.test.VersatileTestRunner;

import static org.junit.Assert.*;

@RunWith(VersatileTestRunner.class)
public class ItemContainerTests {

    @BeforeClass
    public static void before() {
        MinecraftTester.prepare();
    }

    @Test
    public void equalsIdenticalProperties() {
        ItemContainer container1 = new ItemContainer(VanillaItem.DYE.with(ColorType.BLACK));
        ItemContainer container2 = new ItemContainer(VanillaItem.INK_SAC);
        assertEquals(container1, container2);

        ItemContainer container3 = new ItemContainer(VanillaItem.DYE.with(ColorType.BLUE));
        ItemContainer container4 = new ItemContainer(VanillaItem.LAPIS_LAZULI);
        assertEquals(container3, container4);

        ItemContainer container5 = new ItemContainer(VanillaItem.DYE.with(ColorType.WHITE));
        ItemContainer container6 = new ItemContainer(VanillaItem.BONE_MEAL);
        assertEquals(container5, container6);

        ItemContainer container7 = new ItemContainer(VanillaItem.DYE.with(ColorType.BROWN));
        ItemContainer container8 = new ItemContainer(VanillaItem.COCOA_BEANS);
        assertEquals(container7, container8);

        ColorType[] values = ColorType.values();
        for (ColorType type : values) {
            ItemContainer item1 = new ItemContainer(VanillaItem.DYE.with(type));
            ItemContainer item2 = new ItemContainer(new PropertiedItem(VanillaItem.DYE.orEmpty(), type));
            assertEquals(item1, item2);
        }

        ItemContainer container9 = new ItemContainer(VanillaItem.BOAT.with(WoodType.JUNGLE));
        ItemContainer container10 = new ItemContainer(new PropertiedItem(VanillaItem.BOAT.orEmpty(), WoodType.JUNGLE));
        assertEquals(container9, container10);
    }

    @Test
    public void notEqualsDifferentProperties() {
        ItemContainer container1 = new ItemContainer(VanillaItem.DYE.with(ColorType.BLACK));
        ItemContainer container2 = new ItemContainer(VanillaItem.LAPIS_LAZULI);
        assertNotEquals(container1, container2);

        ItemContainer container3 = new ItemContainer(VanillaItem.DYE.with(ColorType.BLUE));
        ItemContainer container4 = new ItemContainer(VanillaItem.BONE_MEAL);
        assertNotEquals(container3, container4);

        ItemContainer container5 = new ItemContainer(VanillaItem.DYE.with(ColorType.WHITE));
        ItemContainer container6 = new ItemContainer(VanillaItem.COCOA_BEANS);
        assertNotEquals(container5, container6);

        ItemContainer container7 = new ItemContainer(VanillaItem.DYE.with(ColorType.BROWN));
        ItemContainer container8 = new ItemContainer(VanillaItem.INK_SAC);
        assertNotEquals(container7, container8);

        ColorType[] values = ColorType.values();
        for (ColorType type1 : values) {
            for (ColorType type2 : values) {
                if (type1.equals(type2))
                    continue;
                ItemContainer item1 = new ItemContainer(VanillaItem.DYE.with(type1));
                ItemContainer item2 = new ItemContainer(VanillaItem.DYE.with(type2));
                assertNotEquals(item1, item2);
            }
        }

        ItemContainer container9 = new ItemContainer(VanillaItem.BOAT.with(WoodType.JUNGLE));
        ItemContainer container10 = new ItemContainer(VanillaItem.BOAT.with(WoodType.DARK_OAK));
        assertNotEquals(container9, container10);
    }

    @Test
    public void hasProperty() {
        ItemContainer container0 = new ItemContainer(VanillaItem.DYE.with(ColorType.BLUE));
        assertTrue(container0.hasProperty(ColorType.BLUE));

        ItemContainer container1 = new ItemContainer(VanillaItem.DYE.with(ColorType.BLACK));
        assertTrue(container1.hasProperty(ColorType.BLACK));

        ItemContainer container3 = new ItemContainer(VanillaItem.BOAT.with(WoodType.JUNGLE));
        assertTrue(container3.hasProperty(WoodType.JUNGLE));


        ItemContainer container10 = new ItemContainer(VanillaItem.DYE.with(ColorType.WHITE));
        assertFalse(container10.hasProperty(ColorType.RED));

        ItemContainer container11 = new ItemContainer(VanillaItem.BOAT.with(WoodType.DARK_OAK));
        assertFalse(container11.hasProperty(WoodType.OAK));

        ItemContainer container12 = new ItemContainer(VanillaItem.ARMOR_STAND);
        assertFalse(container12.hasProperty(ColorType.BROWN));

        ItemContainer container13 = new ItemContainer(VanillaItem.BEETROOT);
        assertFalse(container13.hasProperty(ColorType.PURPLE));

        ItemContainer container14 = new ItemContainer(VanillaItem.STICK);
        assertFalse(container14.hasProperty(ColorType.DEFAULT));

        ItemContainer container15 = new ItemContainer(VanillaItem.BOWL);
        assertFalse(container15.hasProperty(WoodType.DEFAULT));
    }

    @Test
    public void testEquals() {
        ItemContainer container0 = new ItemContainer(VanillaItem.BRICK);
        ItemContainer container1 = new ItemContainer(VanillaItem.STICK);

        assertNotEquals(container0, container1);

        ItemContainer container2 = new ItemContainer(VanillaItem.BOOK);
        ItemContainer container3 = new ItemContainer(VanillaItem.CHAINMAIL_BOOTS);

        assertNotEquals(container2, container3);


        ItemContainer container4 = new ItemContainer(VanillaItem.BLAZE_POWDER);
        ItemContainer container5 = new ItemContainer(VanillaItem.BLAZE_POWDER);

        assertEquals(container4, container5);


        ItemContainer container6 = new ItemContainer(VanillaItem.COOKED_MUTTON);
        ItemContainer container7 = new ItemContainer(VanillaItem.COOKED_MUTTON, 1);

        assertEquals(container6, container7);

        ItemContainer container8 = new ItemContainer(VanillaItem.IRON_INGOT);
        container8.getData();
        ItemContainer container9 = new ItemContainer(VanillaItem.IRON_INGOT, 1);

        assertEquals(container8, container9);

        ItemContainer container10 = new ItemContainer(VanillaItem.COOKED_CHICKEN);
        container10.getData();
        ItemContainer container11 = new ItemContainer(VanillaItem.COOKED_CHICKEN, 1);
        container11.getData();

        assertEquals(container10, container11);

        ItemContainer container12 = new ItemContainer(VanillaItem.BEETROOT_SEEDS);
        container12.getData().set("test", 10);
        ItemContainer container13 = new ItemContainer(VanillaItem.BEETROOT_SEEDS, 1);
        container13.getData().set("test", 10);

        assertEquals(container12, container13);

        ItemContainer container14 = new ItemContainer(VanillaItem.DIAMOND_HELMET);
        container14.getData().set("test", 10);
        ItemContainer container15 = new ItemContainer(VanillaItem.DIAMOND_HELMET, 1);
        container15.getData().set("test2", 10);

        assertNotEquals(container14, container15);

        ItemContainer container16 = new ItemContainer(VanillaItem.COOKED_RABBIT, 24);
        container16.getData().set("test", 10);
        ItemContainer container17 = new ItemContainer(VanillaItem.COOKED_RABBIT, 51);
        container17.getData().set("test2", 10);

        assertNotEquals(container16, container17);

        ItemContainer container18 = new ItemContainer(VanillaItem.BEETROOT_SEEDS, 1);
        container18.getData().set("test", TagCompound.create());
        ItemContainer container19 = new ItemContainer(VanillaItem.BOOK, 1);
        container19.getData().set("test", TagCompound.create());

        assertNotEquals(container18, container19);

        ItemContainer container20 = new ItemContainer(VanillaItem.STICK);
        ItemContainer container21 = new ItemContainer(VanillaItem.STICK);

        assertEquals(container20, container21);

        ItemContainer container22 = new ItemContainer(VanillaItem.STONE_AXE);
        ItemContainer container23 = new ItemContainer(VanillaItem.STONE_AXE);

        assertEquals(container22, container23);
    }

    @Test
    public void testHashCode() {
        ItemContainer container0 = new ItemContainer(VanillaItem.BRICK);
        ItemContainer container1 = new ItemContainer(VanillaItem.BRICK);

        assertEquals(container0.hashCode(), container1.hashCode());


        ItemContainer container2 = new ItemContainer(VanillaItem.STICK, 5);
        ItemContainer container3 = new ItemContainer(VanillaItem.STICK, 5);

        assertEquals(container2.hashCode(), container3.hashCode());

        ItemContainer container4 = new ItemContainer(VanillaItem.SNOWBALL);
        container4.getData();
        ItemContainer container5 = new ItemContainer(VanillaItem.SNOWBALL, 1);

        assertEquals(container4.hashCode(), container5.hashCode());

        ItemContainer container6 = new ItemContainer(VanillaItem.BED);
        container6.getData();
        ItemContainer container7 = new ItemContainer(VanillaItem.BED, 1);
        container7.getData();

        assertEquals(container6.hashCode(), container7.hashCode());

        ItemContainer container8 = new ItemContainer(VanillaItem.COOKED_CHICKEN, 13);
        container8.getData().set("test0", 1);
        ItemContainer container9 = new ItemContainer(VanillaItem.COOKED_CHICKEN,  13);
        container9.getData().set("test0", 1);

        assertEquals(container8.hashCode(), container9.hashCode());

        ItemContainer container10 = new ItemContainer(VanillaItem.DYE.with(ColorType.BLACK), 8);
        container10.getData().set("test0", 1);
        ItemContainer container11 = new ItemContainer(VanillaItem.INK_SAC,  8);
        container11.getData().set("test0", 1);

        assertEquals(container10.hashCode(), container11.hashCode());

        ItemContainer container12 = new ItemContainer(VanillaItem.DYE.with(ColorType.BLUE), 5);
        container12.getData().set("test1", 1);
        container12.getData().set("test2", 11);
        ItemContainer container13 = new ItemContainer(VanillaItem.LAPIS_LAZULI,  5);
        container13.getData().set("test1", 1);
        container13.getData().set("test2", 11);

        assertEquals(container12.hashCode(), container13.hashCode());
    }

    @Test
    public void testIsItemEqual() {
        ItemContainer container0 = new ItemContainer(VanillaItem.BRICK);
        assertTrue(container0.isItemEqual(VanillaItem.BRICK.orEmpty()));

        ItemContainer container1 = new ItemContainer(VanillaItem.APPLE, 3);
        assertTrue(container1.isItemEqual(VanillaItem.APPLE.orEmpty()));

        ItemContainer container2 = new ItemContainer(VanillaItem.BOAT, 1);
        assertFalse(container2.isItemEqual(VanillaItem.APPLE.orEmpty()));

        ItemContainer container3 = new ItemContainer(VanillaItem.INK_SAC, 3);
        assertFalse(container3.isItemEqual(VanillaItem.BOAT.orEmpty()));

        ItemContainer container4 = new ItemContainer(VanillaItem.BOAT.with(WoodType.JUNGLE), 1);
        assertTrue(container4.isItemEqual(VanillaItem.BOAT.orEmpty()));

        ItemContainer container5 = new ItemContainer(VanillaItem.DYE.with(ColorType.WHITE), 14);
        assertFalse(container5.isItemEqual(VanillaItem.INK_SAC.orEmpty()));

        ItemContainer container6 = new ItemContainer(VanillaItem.DYE.with(ColorType.WHITE), 17);
        assertTrue(container6.isItemEqual(VanillaItem.BONE_MEAL));

        ItemContainer container7 = new ItemContainer(VanillaItem.DYE.with(ColorType.WHITE), 1);
        assertTrue(container7.isItemEqual(VanillaItem.DYE));

        ItemContainer container8 = new ItemContainer(VanillaItem.INK_SAC, 1);
        assertTrue(container8.isItemEqual(VanillaItem.DYE));

        ItemContainer container9 = new ItemContainer(VanillaItem.INK_SAC, 1);
        assertFalse(container9.isItemEqual(VanillaItem.BONE_MEAL));

        ItemContainer container10 = new ItemContainer(VanillaItem.COCOA_BEANS, 1);
        container10.getData().set("test0", 13.f);
        assertFalse(container10.isItemEqual(VanillaItem.INK_SAC));
    }

    @Test
    public void testIncrease() {
        ItemContainer container0 = new ItemContainer(VanillaItem.BRICK);
        container0.increase(-1);

        assertEquals(container0, ItemContainer.EMPTY);

        ItemContainer container1 = new ItemContainer(VanillaItem.BEETROOT_SEEDS, 13);
        container1.increase(12);

        assertEquals(container1.getCount(), 12 + 13);

        ItemContainer container2 = new ItemContainer(VanillaItem.BRICK);
        container2.increase(-1);
        container2.increase(10);

        assertEquals(container2, ItemContainer.EMPTY);
        assertEquals(container2.getCount(), 0);
    }
}